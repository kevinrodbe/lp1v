package Controller;
public class Cliente {
	protected int codCliente;
	protected String apeCliente;
	protected String nameCliente;
	protected String fonoCliente;
	protected int estado;
	
	public Cliente() {
		super();
	}

	public Cliente(int codCliente, String apeCliente, String nameCliente,
			String fonoCliente, int estado) {
		super();
		this.codCliente = codCliente;
		this.apeCliente = apeCliente;
		this.nameCliente = nameCliente;
		this.fonoCliente = fonoCliente;
		this.estado = estado;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public String getApeCliente() {
		return apeCliente;
	}

	public void setApeCliente(String apeCliente) {
		this.apeCliente = apeCliente;
	}

	public String getNameCliente() {
		return nameCliente;
	}

	public void setNameCliente(String nameCliente) {
		this.nameCliente = nameCliente;
	}

	public String getFonoCliente() {
		return fonoCliente;
	}

	public void setFonoCliente(String fonoCliente) {
		this.fonoCliente = fonoCliente;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
}
