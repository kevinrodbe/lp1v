package Controller;
public class Atencion {
	private int codAtencion;
	private int codCita;
	private int codCliente;
	private int codMascota;
	private int codCajero;
	private int tipo;
	private String diagnostico;
	private String fechaRegistro;
	private int estado;
	
	public Atencion() {
		super();
	}

	public Atencion(int codAtencion, int codCita, int codCliente,
			int codMascota, int codCajero, int tipo, String diagnostico,
			String fechaRegistro, int estado) {
		super();
		this.codAtencion = codAtencion;
		this.codCita = codCita;
		this.codCliente = codCliente;
		this.codMascota = codMascota;
		this.codCajero = codCajero;
		this.tipo = tipo;
		this.diagnostico = diagnostico;
		this.fechaRegistro = fechaRegistro;
		this.estado = estado;
	}

	public int getCodAtencion() {
		return codAtencion;
	}

	public void setCodAtencion(int codAtencion) {
		this.codAtencion = codAtencion;
	}

	public int getCodCita() {
		return codCita;
	}

	public void setCodCita(int codCita) {
		this.codCita = codCita;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public int getCodMascota() {
		return codMascota;
	}

	public void setCodMascota(int codMascota) {
		this.codMascota = codMascota;
	}

	public int getCodCajero() {
		return codCajero;
	}

	public void setCodCajero(int codCajero) {
		this.codCajero = codCajero;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
}