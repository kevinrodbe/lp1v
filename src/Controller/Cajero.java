package Controller;
public class Cajero {
	private int codCajero;
	private String apeCajero;
	private String nameCajero;
	private String fonoCajero;
	private String loginCajero;
	private String pwdCajero;
	private int turnoCajero;
	
	public Cajero(int codCajero, String apeCajero, String nameCajero,
			String fonoCajero, String loginCajero, String pwdCajero,
			int turnoCajero) {
		super();
		this.codCajero = codCajero;
		this.apeCajero = apeCajero;
		this.nameCajero = nameCajero;
		this.fonoCajero = fonoCajero;
		this.loginCajero = loginCajero;
		this.pwdCajero = pwdCajero;
		this.turnoCajero = turnoCajero;
	}
	public Cajero() {
		super();
	}
	
	public int getCodCajero() {
		return codCajero;
	}
	public void setCodCajero(int codCajero) {
		this.codCajero = codCajero;
	}
	public String getApeCajero() {
		return apeCajero;
	}
	public void setApeCajero(String apeCajero) {
		this.apeCajero = apeCajero;
	}
	public String getNameCajero() {
		return nameCajero;
	}
	public void setNameCajero(String nameCajero) {
		this.nameCajero = nameCajero;
	}
	public String getFonoCajero() {
		return fonoCajero;
	}
	public void setFonoCajero(String fonoCajero) {
		this.fonoCajero = fonoCajero;
	}
	public String getLoginCajero() {
		return loginCajero;
	}
	public void setLoginCajero(String loginCajero) {
		this.loginCajero = loginCajero;
	}
	public String getPwdCajero() {
		return pwdCajero;
	}
	public void setPwdCajero(String pwdCajero) {
		this.pwdCajero = pwdCajero;
	}
	public int getTurnoCajero() {
		return turnoCajero;
	}
	public void setTurnoCajero(int turnoCajero) {
		this.turnoCajero = turnoCajero;
	}
}
