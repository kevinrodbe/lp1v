package Controller;
public class Mascota{
	private int codMascota;
	private int codCliente;
	private String nameMascota;
	private String tipo;
	private String raza;
	private String fechaNacimiento;
	private int estado;
	public Mascota() {
		super();
	}
	public Mascota(int codMascota, int codCliente, String nameMascota,
			String tipo, String raza, String fechaNacimiento, int estado) {
		super();
		this.codMascota = codMascota;
		this.codCliente = codCliente;
		this.nameMascota = nameMascota;
		this.tipo = tipo;
		this.raza = raza;
		this.fechaNacimiento = fechaNacimiento;
		this.estado = estado;
	}
	public int getCodMascota() {
		return codMascota;
	}
	public void setCodMascota(int codMascota) {
		this.codMascota = codMascota;
	}
	public int getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}
	public String getNameMascota() {
		return nameMascota;
	}
	public void setNameMascota(String nameMascota) {
		this.nameMascota = nameMascota;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
}