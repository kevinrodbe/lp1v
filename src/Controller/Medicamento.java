package Controller;
public class Medicamento {
	private int codMedicamento;
	private String nameMedicamento;
	private String laboratorio;
	private double preUni;
	private int stock;
	
	public Medicamento() {
		super();
	}

	public Medicamento(int codMedicamento, String nameMedicamento,
			String laboratorio, double preUni, int stock) {
		super();
		this.codMedicamento = codMedicamento;
		this.nameMedicamento = nameMedicamento;
		this.laboratorio = laboratorio;
		this.preUni = preUni;
		this.stock = stock;
	}

	public int getCodMedicamento() {
		return codMedicamento;
	}

	public void setCodMedicamento(int codMedicamento) {
		this.codMedicamento = codMedicamento;
	}

	public String getNameMedicamento() {
		return nameMedicamento;
	}

	public void setNameMedicamento(String nameMedicamento) {
		this.nameMedicamento = nameMedicamento;
	}

	public String getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}

	public double getPreUni() {
		return preUni;
	}

	public void setPreUni(double preUni) {
		this.preUni = preUni;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}