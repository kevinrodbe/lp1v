package Controller;
public class Receta {
	private int codAtencion;
	private int codMedicamento;
	private int cantidad;
	private double precio;
	
	public Receta() {
		super();
	}

	public Receta(int codAtencion, int codMedicamento, int cantidad,
			double precio) {
		super();
		this.codAtencion = codAtencion;
		this.codMedicamento = codMedicamento;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public int getCodAtencion() {
		return codAtencion;
	}

	public void setCodAtencion(int codAtencion) {
		this.codAtencion = codAtencion;
	}

	public int getCodMedicamento() {
		return codMedicamento;
	}

	public void setCodMedicamento(int codMedicamento) {
		this.codMedicamento = codMedicamento;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
}