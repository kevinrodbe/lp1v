package Controller;
public class Cita {
	private int codCita;
	private int codCliente;
	private int codCajero;
	private int codMascota;
	private String fechaRegistro;
	private String fechaAtencion;
	private int estado;
	
	public Cita() {
		super();
	}

	public Cita(int codCita, int codCliente, int codCajero, int codMascota,
			String fechaRegistro, String fechaAtencion, int estado) {
		super();
		this.codCita = codCita;
		this.codCliente = codCliente;
		this.codCajero = codCajero;
		this.codMascota = codMascota;
		this.fechaRegistro = fechaRegistro;
		this.fechaAtencion = fechaAtencion;
		this.estado = estado;
	}

	public int getCodCita() {
		return codCita;
	}

	public void setCodCita(int codCita) {
		this.codCita = codCita;
	}

	public int getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(int codCliente) {
		this.codCliente = codCliente;
	}

	public int getCodCajero() {
		return codCajero;
	}

	public void setCodCajero(int codCajero) {
		this.codCajero = codCajero;
	}

	public int getCodMascota() {
		return codMascota;
	}

	public void setCodMascota(int codMascota) {
		this.codMascota = codMascota;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(String fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
}