package Model;
import java.util.ArrayList;
import Controller.Cita;
import java.io.*;
import java.util.StringTokenizer;

public class ArregloCita {
	private ArrayList<Cita> aCita;
	
	public ArregloCita() {
		aCita=new ArrayList<Cita>();		
		cargarCita();
	}
	
	public void agregar(Cita c) {
		aCita.add(c);		
	}
	
	public Cita obtener(int index) {
		return aCita.get(index);		
	}
	
	public int tama�o() {
		return aCita.size();		
	}
	
	public Cita buscar(int cod) {
		for (Cita c : aCita) {
			if (c.getCodCita()==cod) {
				return c;				
			}			
		}
		return null;
	}
	
	public void eliminar(Cita c) {
		aCita.remove(c);		
	}
	
	public void grabarCita(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("Cita.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodCita()+", "+
						obtener(i).getCodCliente()+", "+
						obtener(i).getCodCajero()+", "+
						obtener(i).getCodMascota()+", "+
						obtener(i).getFechaRegistro()+", "+
						obtener(i).getFechaAtencion()+", "+
						obtener(i).getEstado();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarCita(){
		try {
			File archivo=new File("Cita.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("Cita.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codCita=Integer.parseInt(st.nextToken().trim());
					int codCliente=Integer.parseInt(st.nextToken().trim());
					int codCajero=Integer.parseInt(st.nextToken().trim());
					int codMascota=Integer.parseInt(st.nextToken().trim());
					String fReg=st.nextToken().trim();
					String fAten=st.nextToken().trim();					
					int estado=Integer.parseInt(st.nextToken().trim());					
					Cita objC=new Cita(codCita,codCliente,codCajero,codMascota,fReg,fAten,estado);
					agregar(objC);
				}
				br.close();
			}else{
				System.out.println("el archivo Cita.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}
}
