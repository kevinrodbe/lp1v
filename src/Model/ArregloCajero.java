package Model;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.*;

import Controller.*;

public class ArregloCajero {	
	private ArrayList<Cajero> aCajero;
	
	public ArregloCajero() {
		aCajero=new ArrayList<Cajero>();		
		cargarCajero();
	}
	
	public void agregar(Cajero c) {
		aCajero.add(c);		
	}
	
	public Cajero obtener(int index) {
		return aCajero.get(index);		
	}
	
	public int tama�o() {
		return aCajero.size();		
	}
	
	public Cajero buscar(int cod) {
		for (Cajero c : aCajero) {
			if (c.getCodCajero()==cod) {
				return c;				
			}			
		}
		return null;
	}
	
	public void eliminar(Cajero c) {
		aCajero.remove(c);		
	}
	
	public void grabarCajero(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("cajeros.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodCajero()+", "+
						obtener(i).getApeCajero()+", "+
						obtener(i).getNameCajero()+", "+
						obtener(i).getFonoCajero()+", "+
						obtener(i).getLoginCajero()+", "+
						obtener(i).getPwdCajero()+", "+
						obtener(i).getTurnoCajero();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarCajero(){
		try {
			File archivo=new File("cajeros.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("cajeros.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int cod=Integer.parseInt(st.nextToken().trim());					
					String ape=st.nextToken().trim();
					String name=st.nextToken().trim();
					String fono=st.nextToken().trim();
					String login=st.nextToken().trim();
					String pwd=st.nextToken().trim();
					int turno=Integer.parseInt(st.nextToken().trim());	
					
					Cajero objC=new Cajero(cod,ape,name,fono,login,pwd,turno);
					agregar(objC);
				}
				br.close();
			}else{
				System.out.println("el archivo cajeros.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}
  
}
