package Model;
import java.util.ArrayList;
import java.util.StringTokenizer;
import Controller.*;
import java.io.*;
public class ArregloMascota {
	private ArrayList<Mascota> aMascota;
	
	public ArregloMascota() {
		aMascota=new ArrayList<Mascota>();
		cargarMascota();
	}
	
	public void agregar(Mascota m) {
		aMascota.add(m);		
	}
	
	public Mascota obtener(int index) {
		return aMascota.get(index);		
	}
	
	public int tama�o() {
		return aMascota.size();		
	}
	
	public Mascota buscar(int cod) {
		for (Mascota m : aMascota) {
			if (m.getCodMascota()==cod) {
				return m;				
			}			
		}
		return null;
	}
	
	public void eliminar(Mascota m) {
		aMascota.remove(m);		
	}
	
	public void grabarMascota(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("mascotas.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodMascota()+", "+
						obtener(i).getCodCliente()+", "+
						obtener(i).getNameMascota()+", "+
						obtener(i).getTipo()+", "+
						obtener(i).getRaza()+", "+
						obtener(i).getFechaNacimiento()+", "+
						obtener(i).getEstado();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarMascota(){
		try {
			File archivo=new File("mascotas.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("mascotas.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codMas=Integer.parseInt(st.nextToken().trim());
					int codCli=Integer.parseInt(st.nextToken().trim());					
					String name=st.nextToken().trim();
					String tipo=st.nextToken().trim();
					String raza=st.nextToken().trim();
					String fecNac=st.nextToken().trim();
					int estado=Integer.parseInt(st.nextToken().trim());	
					
					Mascota objM=new Mascota(codMas,codCli,name,tipo,raza,fecNac,estado);
					agregar(objM);
				}
				br.close();
			}else{
				System.out.println("el archivo mascotas.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}
}
