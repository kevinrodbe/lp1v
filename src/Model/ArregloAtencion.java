package Model;
import java.util.ArrayList;
import Controller.Atencion;
import java.io.*;
import java.util.StringTokenizer;

public class ArregloAtencion {
	private ArrayList<Atencion> aAtencion;
	
	public ArregloAtencion() {
		aAtencion=new ArrayList<Atencion>();
		cargarCita();
	}
	
	public void agregar(Atencion a) {
		aAtencion.add(a);		
	}
	
	public Atencion obtener(int index) {
		return aAtencion.get(index);		
	}
	
	public int tama�o() {
		return aAtencion.size();		
	}
	
	public Atencion buscar(int cod) {
		for (Atencion a : aAtencion) {
			if (a.getCodAtencion()==cod) {
				return a;				
			}			
		}
		return null;
	}
	
	public void eliminar(Atencion a) {
		aAtencion.remove(a);		
	}
	
	public void grabarAtencion(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("Atencion.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodAtencion()+", "+
						obtener(i).getCodCita()+", "+
						obtener(i).getCodCliente()+", "+
						obtener(i).getCodMascota()+", "+
						obtener(i).getCodCajero()+", "+
						obtener(i).getTipo()+", "+
						obtener(i).getDiagnostico()+", "+
						obtener(i).getFechaRegistro()+", "+
						obtener(i).getEstado();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarCita(){
		try {
			File archivo=new File("Atencion.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("Atencion.txt"));
				String linea;
				while((linea=br.readLine())!=null){					
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codAtencion=Integer.parseInt(st.nextToken().trim());
					int codCita=Integer.parseInt(st.nextToken().trim());
					int codCliente=Integer.parseInt(st.nextToken().trim());
					int codMascota=Integer.parseInt(st.nextToken().trim());
					int codCajero=Integer.parseInt(st.nextToken().trim());
					int tipo=Integer.parseInt(st.nextToken().trim());					
					String diagnostico=st.nextToken().trim();
					String fReg=st.nextToken().trim();					
					int estado=Integer.parseInt(st.nextToken().trim());					
					Atencion objA=new Atencion(codAtencion,codCita,codCliente,codMascota,codCajero,tipo,diagnostico,fReg,estado);
					agregar(objA);
				}
				br.close();
			}else{
				System.out.println("el archivo Atencion.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}

}
