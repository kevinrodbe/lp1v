package Model;
import java.util.ArrayList;
import Controller.Medicamento;
import java.io.*;
import java.util.StringTokenizer;

public class ArregloMedicamento {
	private ArrayList<Medicamento> aMedicamento;
	
	public ArregloMedicamento() {
		aMedicamento=new ArrayList<Medicamento>();
		cargarMedicamento();
	}
	
	public void agregar(Medicamento m) {
		aMedicamento.add(m);		
	}
	
	public Medicamento obtener(int index) {
		return aMedicamento.get(index);		
	}
	
	public int tama�o() {
		return aMedicamento.size();		
	}
	
	public Medicamento buscar(int cod) {
		for (Medicamento m : aMedicamento) {
			if (m.getCodMedicamento()==cod) {
				return m;				
			}			
		}
		return null;
	}
	
	public void eliminar(Medicamento m) {
		aMedicamento.remove(m);		
	}
	
	public void grabarMedicamento(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("medicamento.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodMedicamento()+", "+
						obtener(i).getNameMedicamento()+", "+
						obtener(i).getLaboratorio()+", "+
						obtener(i).getPreUni()+", "+
						obtener(i).getStock();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarMedicamento(){
		try {
			File archivo=new File("medicamento.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("medicamento.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codMed=Integer.parseInt(st.nextToken().trim());				
					String name=st.nextToken().trim();
					String lab=st.nextToken().trim();
					double preUni= Double.parseDouble(st.nextToken().trim());
					int stock=Integer.parseInt(st.nextToken().trim());	
					
					Medicamento objM=new Medicamento(codMed,name,lab,preUni,stock);
					agregar(objM);
				}
				br.close();
			}else{
				System.out.println("el archivo Medicamento.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}
}
