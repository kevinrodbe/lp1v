package Model;
import java.util.ArrayList;

import Controller.*;

import java.io.*;
import java.util.StringTokenizer;

public class ArregloReceta {
	private ArrayList<Receta> aReceta;
	
	public ArregloReceta() {
		aReceta=new ArrayList<Receta>();
		cargarReceta();
	}
	
	public void agregar(Receta r) {
		aReceta.add(r);		
	}
	
	public Receta obtener(int index) {
		return aReceta.get(index);		
	}
	
	public int tama�o() {
		return aReceta.size();		
	}
	
	public Receta buscarReceta_x_Atencion(int cod) {
		for (Receta r : aReceta) {
			if (r.getCodAtencion()==cod) {
				return r;				
			}			
		}
		return null;
	}
	public Receta Medicamento(int cod) {
		for (Receta r : aReceta) {
			if (r.getCodMedicamento()==cod) {
				return r;				
			}			
		}
		return null;
	}
	
	public void eliminar(Receta r) {
		aReceta.remove(r);		
	}
	
	public void grabarReceta(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("Receta.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodAtencion()+", "+
						obtener(i).getCodMedicamento()+", "+
						obtener(i).getCantidad()+", "+
						obtener(i).getPrecio();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarReceta(){
		try {
			File archivo=new File("Receta.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("Receta.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codAtencion=Integer.parseInt(st.nextToken().trim());
					int codMedicamento=Integer.parseInt(st.nextToken().trim());
					int cantidad=Integer.parseInt(st.nextToken().trim());			
					double precio=Double.parseDouble(st.nextToken().trim());					
					Receta objR=new Receta(codAtencion,codMedicamento,cantidad,precio);
					agregar(objR);
				}
				br.close();
			}else{
				System.out.println("el archivo Receta.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}

}
