package Model;
import java.util.ArrayList;
import java.util.StringTokenizer;

import Controller.*;

import java.io.*;
public class ArregloCliente {
	private ArrayList<Cliente> aCliente;
	
	public ArregloCliente() {
		aCliente=new ArrayList<Cliente>();
		cargarCliente();
	}
	
	public void agregar(Cliente c) {
		aCliente.add(c);		
	}
	
	public Cliente obtener(int index) {
		return aCliente.get(index);		
	}
	
	public int tama�o() {
		return aCliente.size();		
	}
	
	public Cliente buscar(int cod) {
		for (Cliente c : aCliente) {
			if (c.getCodCliente()==cod) {
				return c;				
			}			
		}
		return null;
	}
	
	public void eliminar(Cliente c) {
		aCliente.remove(c);		
	}
	
	public void grabarCliente(){
		try {
			PrintWriter pw=new PrintWriter(new FileWriter("clientes.txt"));
			String linea;
			for (int i = 0; i < tama�o(); i++) {
				linea=obtener(i).getCodCliente()+", "+
						obtener(i).getApeCliente()+", "+
						obtener(i).getNameCliente()+", "+
						obtener(i).getFonoCliente()+", "+
						obtener(i).getEstado();
				pw.println(linea);
			}
			pw.close();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	public void cargarCliente(){
		try {
			File archivo=new File("clientes.txt");
			if (archivo.exists()) {
				BufferedReader br=new BufferedReader(new FileReader("clientes.txt"));
				String linea;
				while((linea=br.readLine())!=null){
					StringTokenizer st=new StringTokenizer(linea, ", ");
					int codCli=Integer.parseInt(st.nextToken().trim());										
					String ape=st.nextToken().trim();
					String name=st.nextToken().trim();
					String fono=st.nextToken().trim();					
					int estado=Integer.parseInt(st.nextToken().trim());	
					
					Cliente objC=new Cliente(codCli,ape,name,fono,estado);
					agregar(objC);
				}
				br.close();
			}else{
				System.out.println("el archivo clientes.txt no existe");
			}
		} catch (Exception e) {
			System.out.println("error: "+e);
		}
	}
}
