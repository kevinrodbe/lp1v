package View;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;

import javax.swing.table.DefaultTableModel;

import Controller.Cajero;
import Model.ArregloCajero;

@SuppressWarnings("serial")
public class P_1_1_1_MantenimientoCajero extends JPanel implements
		ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtApellido;
	private JTextField txtName;
	private JTextField txtFono;
	private JTextField txtPwd;
	private JTextField txtLogin;
	private JTextField txtCodCajero;
	private JTextField txtPwd_pnlModi;
	private JTextField txtFono_pnlModi;
	private JTextField txtApe_pnlModi;
	private JTextField txtLogin_pnlModi;
	private JTextField txtName_pnlModi;
	private JTextField txtCodCajero_Pmodificar;
	private JTextField txtCodCajero_pnlEliminar;
	private JTextField txtPwdCajero_pnlEliminar;
	private JTextField txtFonoCajero_pnlEliminar;
	private JTextField txtApeCajero_pnlEliminar;
	private JTextField txtLoginCajero_pnlEliminar;
	private JTextField txtName_pnlEliminar;
	private JTextField txtTurnoCajero_pnlEliminar;
	private JTextField txtCodCajero_pnlConsultar;
	private JTextField txtApeCajero_pnlConsultar;
	private JTextField txtFonoCajero_pnlConsultar;
	private JTextField txtPwdCajero_pnlConsultar;
	private JTextField txtLoginCajero_pnlConsultar;
	private JTextField txtTurnoCajero_pnlConsultar;
	private JTextField txtNameCajero_pnlConsultar;
	private JLabel lblSalir_1_1_1;

	public ArregloCajero aCajero = new ArregloCajero();
	private JComboBox cboTurno;
	private JButton btnRegistrar;
	private JLabel lbl_estadoIngresar;
	private JLabel lbl_estadoModificar;
	private JLabel lbl_estadoEliminar;
	private JLabel lbl_estadoConsultar;
	private JComboBox cboTurno_pnlModi;
	private JTextArea txtS_Cajeros;

	public P_1_1_1_MantenimientoCajero(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(495, 335);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Mantenimiento - Cajeros", TitledBorder.LEADING,
				TitledBorder.TOP, null, Color.BLACK));

		JLabel lblMantenimientoCajeros = new JLabel("Mantenimiento - Cajeros");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		lblSalir_1_1_1 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_1_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		lblSalir_1_1_1.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_1_1.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_1_1.setBounds(10, 289, 70, 33);
		add(lblSalir_1_1_1);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 475, 220);
		add(tabbedPane);

		JPanel pnl_Ingresar_Manto = new JPanel();
		pnl_Ingresar_Manto.setToolTipText("");
		tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Manto, null);
		pnl_Ingresar_Manto.setLayout(null);

		JLabel lblIngresarCajeros = new JLabel("Ingresar Cajeros");
		lblIngresarCajeros.setBounds(10, 11, 171, 22);
		lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarCajeros.setForeground(Color.BLUE);
		lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblIngresarCajeros.setAlignmentX(0.5f);
		pnl_Ingresar_Manto.add(lblIngresarCajeros);

		JLabel lblNewLabel = new JLabel("Apellido");
		lblNewLabel.setBounds(20, 44, 65, 14);
		pnl_Ingresar_Manto.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(239, 44, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Tel\u00E9fono");
		lblNewLabel_2.setBounds(20, 74, 65, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Login");
		lblNewLabel_3.setBounds(239, 104, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_3);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(20, 104, 65, 14);
		pnl_Ingresar_Manto.add(lblPassword);

		JLabel lblNewLabel_4 = new JLabel("Turno");
		lblNewLabel_4.setBounds(239, 74, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_4);

		txtApellido = new JTextField();
		txtApellido.setBounds(95, 40, 134, 20);
		pnl_Ingresar_Manto.add(txtApellido);
		txtApellido.setColumns(10);

		txtName = new JTextField();
		txtName.setBounds(295, 40, 165, 20);
		pnl_Ingresar_Manto.add(txtName);
		txtName.setColumns(10);

		txtFono = new JTextField();
		txtFono.setBounds(95, 70, 134, 20);
		pnl_Ingresar_Manto.add(txtFono);
		txtFono.setColumns(10);

		txtPwd = new JTextField();
		txtPwd.setColumns(10);
		txtPwd.setBounds(95, 100, 134, 20);
		pnl_Ingresar_Manto.add(txtPwd);

		txtLogin = new JTextField();
		txtLogin.setColumns(10);
		txtLogin.setBounds(295, 100, 165, 20);
		pnl_Ingresar_Manto.add(txtLogin);

		cboTurno = new JComboBox();
		cboTurno.setModel(new DefaultComboBoxModel(new String[] { "Noche",
				"Dia" }));
		cboTurno.setBounds(295, 70, 165, 20);
		pnl_Ingresar_Manto.add(cboTurno);

		btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				grabarCajero();
			}
		});
		btnRegistrar.setBounds(295, 142, 165, 40);
		pnl_Ingresar_Manto.add(btnRegistrar);

		JLabel lblCdigo = new JLabel("C\u00F3digo");
		lblCdigo.setBounds(239, 14, 46, 14);
		pnl_Ingresar_Manto.add(lblCdigo);

		txtCodCajero = new JTextField();
		txtCodCajero.setEditable(false);
		txtCodCajero.setBounds(295, 14, 165, 20);
		pnl_Ingresar_Manto.add(txtCodCajero);
		txtCodCajero.setColumns(10);

		JPanel pnl_Modificar_Manto = new JPanel();
		tabbedPane.addTab("Modificar", null, pnl_Modificar_Manto, null);
		pnl_Modificar_Manto.setLayout(null);

		JLabel lblModificarCajeros = new JLabel("Modificar Cajeros");
		lblModificarCajeros.setBounds(10, 11, 175, 22);
		lblModificarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblModificarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblModificarCajeros.setForeground(Color.BLUE);
		lblModificarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblModificarCajeros.setAlignmentX(0.5f);
		pnl_Modificar_Manto.add(lblModificarCajeros);

		JLabel label_1 = new JLabel("Apellido");
		label_1.setBounds(20, 55, 65, 14);
		pnl_Modificar_Manto.add(label_1);

		JLabel label_2 = new JLabel("Tel\u00E9fono");
		label_2.setBounds(20, 85, 65, 14);
		pnl_Modificar_Manto.add(label_2);

		JLabel label_3 = new JLabel("Password");
		label_3.setBounds(20, 115, 65, 14);
		pnl_Modificar_Manto.add(label_3);

		txtPwd_pnlModi = new JTextField();
		txtPwd_pnlModi.setColumns(10);
		txtPwd_pnlModi.setBounds(95, 111, 134, 20);
		pnl_Modificar_Manto.add(txtPwd_pnlModi);

		txtFono_pnlModi = new JTextField();
		txtFono_pnlModi.setColumns(10);
		txtFono_pnlModi.setBounds(95, 81, 134, 20);
		pnl_Modificar_Manto.add(txtFono_pnlModi);

		txtApe_pnlModi = new JTextField();
		txtApe_pnlModi.setColumns(10);
		txtApe_pnlModi.setBounds(95, 51, 134, 20);
		pnl_Modificar_Manto.add(txtApe_pnlModi);

		JLabel label_4 = new JLabel("C\u00F3digo");
		label_4.setBounds(239, 18, 46, 14);
		pnl_Modificar_Manto.add(label_4);

		JLabel label_5 = new JLabel("Nombre");
		label_5.setBounds(239, 55, 46, 14);
		pnl_Modificar_Manto.add(label_5);

		JLabel label_6 = new JLabel("Turno");
		label_6.setBounds(239, 85, 46, 14);
		pnl_Modificar_Manto.add(label_6);

		JLabel label_7 = new JLabel("Login");
		label_7.setBounds(239, 115, 46, 14);
		pnl_Modificar_Manto.add(label_7);

		txtLogin_pnlModi = new JTextField();
		txtLogin_pnlModi.setColumns(10);
		txtLogin_pnlModi.setBounds(295, 111, 165, 20);
		pnl_Modificar_Manto.add(txtLogin_pnlModi);

		cboTurno_pnlModi = new JComboBox();
		cboTurno_pnlModi.setModel(new DefaultComboBoxModel(new String[] {
				"Noche", "Dia" }));
		cboTurno_pnlModi.setBounds(295, 81, 165, 20);
		pnl_Modificar_Manto.add(cboTurno_pnlModi);

		txtName_pnlModi = new JTextField();
		txtName_pnlModi.setColumns(10);
		txtName_pnlModi.setBounds(295, 51, 165, 20);
		pnl_Modificar_Manto.add(txtName_pnlModi);

		txtCodCajero_Pmodificar = new JTextField();
		txtCodCajero_Pmodificar.setColumns(10);
		txtCodCajero_Pmodificar.setBounds(295, 15, 116, 20);
		pnl_Modificar_Manto.add(txtCodCajero_Pmodificar);

		JButton btnGrabar_pnlModi = new JButton("Grabar");
		btnGrabar_pnlModi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizarCajero();
			}
		});
		btnGrabar_pnlModi.setBounds(295, 142, 165, 40);
		pnl_Modificar_Manto.add(btnGrabar_pnlModi);

		JButton btnBuscar_pnlModi = new JButton("b");
		btnBuscar_pnlModi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				buscarCajero_pnlModificar();
			}
		});
		btnBuscar_pnlModi.setBounds(421, 12, 39, 23);
		pnl_Modificar_Manto.add(btnBuscar_pnlModi);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 44, 470, 9);
		pnl_Modificar_Manto.add(separator);

		lbl_estadoModificar = new JLabel("");
		lbl_estadoModificar.setForeground(Color.RED);
		lbl_estadoModificar.setBounds(20, 168, 265, 14);
		pnl_Modificar_Manto.add(lbl_estadoModificar);

		JPanel pnl_Eliminar_Manto = new JPanel();
		tabbedPane.addTab("Eliminar", null, pnl_Eliminar_Manto, null);
		pnl_Eliminar_Manto.setLayout(null);

		JLabel lblEliminarCajeros = new JLabel("Eliminar Cajeros");
		lblEliminarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEliminarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblEliminarCajeros.setForeground(Color.BLUE);
		lblEliminarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblEliminarCajeros.setAlignmentX(0.5f);
		lblEliminarCajeros.setBounds(10, 11, 171, 22);
		pnl_Eliminar_Manto.add(lblEliminarCajeros);

		txtCodCajero_pnlEliminar = new JTextField();
		txtCodCajero_pnlEliminar.setColumns(10);
		txtCodCajero_pnlEliminar.setBounds(295, 14, 116, 20);
		pnl_Eliminar_Manto.add(txtCodCajero_pnlEliminar);

		JLabel label = new JLabel("C\u00F3digo");
		label.setBounds(239, 17, 46, 14);
		pnl_Eliminar_Manto.add(label);

		JButton btnBuscar_pnlEliminar = new JButton("b");
		btnBuscar_pnlEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				buscarCajero_pnlEliminar();
			}
		});
		btnBuscar_pnlEliminar.setBounds(421, 12, 39, 23);
		pnl_Eliminar_Manto.add(btnBuscar_pnlEliminar);

		JLabel label_8 = new JLabel("Apellido");
		label_8.setBounds(20, 54, 68, 14);
		pnl_Eliminar_Manto.add(label_8);

		JLabel label_9 = new JLabel("Tel\u00E9fono");
		label_9.setBounds(20, 84, 68, 14);
		pnl_Eliminar_Manto.add(label_9);

		JLabel label_10 = new JLabel("Password");
		label_10.setBounds(20, 114, 68, 14);
		pnl_Eliminar_Manto.add(label_10);

		txtPwdCajero_pnlEliminar = new JTextField();
		txtPwdCajero_pnlEliminar.setEditable(false);
		txtPwdCajero_pnlEliminar.setColumns(10);
		txtPwdCajero_pnlEliminar.setBounds(98, 110, 131, 20);
		pnl_Eliminar_Manto.add(txtPwdCajero_pnlEliminar);

		txtFonoCajero_pnlEliminar = new JTextField();
		txtFonoCajero_pnlEliminar.setEditable(false);
		txtFonoCajero_pnlEliminar.setColumns(10);
		txtFonoCajero_pnlEliminar.setBounds(98, 80, 131, 20);
		pnl_Eliminar_Manto.add(txtFonoCajero_pnlEliminar);

		txtApeCajero_pnlEliminar = new JTextField();
		txtApeCajero_pnlEliminar.setEditable(false);
		txtApeCajero_pnlEliminar.setColumns(10);
		txtApeCajero_pnlEliminar.setBounds(98, 50, 131, 20);
		pnl_Eliminar_Manto.add(txtApeCajero_pnlEliminar);

		JLabel label_11 = new JLabel("Nombre");
		label_11.setBounds(239, 54, 46, 14);
		pnl_Eliminar_Manto.add(label_11);

		JLabel label_12 = new JLabel("Turno");
		label_12.setBounds(239, 84, 46, 14);
		pnl_Eliminar_Manto.add(label_12);

		JLabel label_13 = new JLabel("Login");
		label_13.setBounds(239, 114, 46, 14);
		pnl_Eliminar_Manto.add(label_13);

		txtLoginCajero_pnlEliminar = new JTextField();
		txtLoginCajero_pnlEliminar.setEditable(false);
		txtLoginCajero_pnlEliminar.setColumns(10);
		txtLoginCajero_pnlEliminar.setBounds(295, 110, 165, 20);
		pnl_Eliminar_Manto.add(txtLoginCajero_pnlEliminar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				eliminarCajero();
			}
		});
		btnEliminar.setBounds(295, 141, 165, 40);
		pnl_Eliminar_Manto.add(btnEliminar);

		txtName_pnlEliminar = new JTextField();
		txtName_pnlEliminar.setEditable(false);
		txtName_pnlEliminar.setColumns(10);
		txtName_pnlEliminar.setBounds(295, 50, 165, 20);
		pnl_Eliminar_Manto.add(txtName_pnlEliminar);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 43, 470, 9);
		pnl_Eliminar_Manto.add(separator_1);

		txtTurnoCajero_pnlEliminar = new JTextField();
		txtTurnoCajero_pnlEliminar.setEditable(false);
		txtTurnoCajero_pnlEliminar.setBounds(295, 80, 165, 20);
		pnl_Eliminar_Manto.add(txtTurnoCajero_pnlEliminar);
		txtTurnoCajero_pnlEliminar.setColumns(10);

		lbl_estadoEliminar = new JLabel("");
		lbl_estadoEliminar.setForeground(Color.RED);
		lbl_estadoEliminar.setBounds(10, 167, 275, 14);
		pnl_Eliminar_Manto.add(lbl_estadoEliminar);

		JPanel pnl_Consultar_Manto = new JPanel();
		tabbedPane.addTab("Consultar", null, pnl_Consultar_Manto, null);
		pnl_Consultar_Manto.setLayout(null);

		JLabel lblConsultarCajeros = new JLabel("Consultar Cajeros");
		lblConsultarCajeros.setBounds(10, 11, 179, 22);
		lblConsultarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblConsultarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsultarCajeros.setForeground(Color.BLUE);
		lblConsultarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblConsultarCajeros.setAlignmentX(0.5f);
		pnl_Consultar_Manto.add(lblConsultarCajeros);

		txtCodCajero_pnlConsultar = new JTextField();
		txtCodCajero_pnlConsultar.setColumns(10);
		txtCodCajero_pnlConsultar.setBounds(295, 13, 116, 20);
		pnl_Consultar_Manto.add(txtCodCajero_pnlConsultar);

		JLabel label_14 = new JLabel("C\u00F3digo");
		label_14.setBounds(239, 16, 46, 14);
		pnl_Consultar_Manto.add(label_14);

		JButton btnBusca_pnlConsultar = new JButton("b");
		btnBusca_pnlConsultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarCajero_pnlConsultar();
			}
		});
		btnBusca_pnlConsultar.setBounds(421, 11, 39, 23);
		pnl_Consultar_Manto.add(btnBusca_pnlConsultar);

		txtApeCajero_pnlConsultar = new JTextField();
		txtApeCajero_pnlConsultar.setEditable(false);
		txtApeCajero_pnlConsultar.setColumns(10);
		txtApeCajero_pnlConsultar.setBounds(106, 62, 123, 20);
		pnl_Consultar_Manto.add(txtApeCajero_pnlConsultar);

		txtFonoCajero_pnlConsultar = new JTextField();
		txtFonoCajero_pnlConsultar.setEditable(false);
		txtFonoCajero_pnlConsultar.setColumns(10);
		txtFonoCajero_pnlConsultar.setBounds(106, 92, 123, 20);
		pnl_Consultar_Manto.add(txtFonoCajero_pnlConsultar);

		txtPwdCajero_pnlConsultar = new JTextField();
		txtPwdCajero_pnlConsultar.setEditable(false);
		txtPwdCajero_pnlConsultar.setColumns(10);
		txtPwdCajero_pnlConsultar.setBounds(106, 122, 123, 20);
		pnl_Consultar_Manto.add(txtPwdCajero_pnlConsultar);

		JLabel label_15 = new JLabel("Password");
		label_15.setBounds(20, 126, 76, 14);
		pnl_Consultar_Manto.add(label_15);

		JLabel label_16 = new JLabel("Tel\u00E9fono");
		label_16.setBounds(20, 96, 76, 14);
		pnl_Consultar_Manto.add(label_16);

		JLabel label_17 = new JLabel("Apellido");
		label_17.setBounds(20, 66, 76, 14);
		pnl_Consultar_Manto.add(label_17);

		JLabel label_18 = new JLabel("Nombre");
		label_18.setBounds(239, 66, 46, 14);
		pnl_Consultar_Manto.add(label_18);

		JLabel label_19 = new JLabel("Turno");
		label_19.setBounds(239, 96, 46, 14);
		pnl_Consultar_Manto.add(label_19);

		JLabel label_20 = new JLabel("Login");
		label_20.setBounds(239, 126, 46, 14);
		pnl_Consultar_Manto.add(label_20);

		txtLoginCajero_pnlConsultar = new JTextField();
		txtLoginCajero_pnlConsultar.setEditable(false);
		txtLoginCajero_pnlConsultar.setColumns(10);
		txtLoginCajero_pnlConsultar.setBounds(295, 122, 165, 20);
		pnl_Consultar_Manto.add(txtLoginCajero_pnlConsultar);

		txtTurnoCajero_pnlConsultar = new JTextField();
		txtTurnoCajero_pnlConsultar.setEditable(false);
		txtTurnoCajero_pnlConsultar.setColumns(10);
		txtTurnoCajero_pnlConsultar.setBounds(295, 92, 165, 20);
		pnl_Consultar_Manto.add(txtTurnoCajero_pnlConsultar);

		txtNameCajero_pnlConsultar = new JTextField();
		txtNameCajero_pnlConsultar.setEditable(false);
		txtNameCajero_pnlConsultar.setColumns(10);
		txtNameCajero_pnlConsultar.setBounds(295, 62, 165, 20);
		pnl_Consultar_Manto.add(txtNameCajero_pnlConsultar);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 42, 470, 9);
		pnl_Consultar_Manto.add(separator_2);

		lbl_estadoConsultar = new JLabel("");
		lbl_estadoConsultar.setForeground(Color.RED);
		lbl_estadoConsultar.setBounds(10, 167, 219, 14);
		pnl_Consultar_Manto.add(lbl_estadoConsultar);

		JPanel pnl_Listar_Manto = new JPanel();
		tabbedPane.addTab("Listar", null, pnl_Listar_Manto, null);
		pnl_Listar_Manto.setLayout(null);

		JLabel lblListarCajeros = new JLabel("Listar Cajeros");
		lblListarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblListarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarCajeros.setForeground(Color.BLUE);
		lblListarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarCajeros.setAlignmentX(0.5f);
		lblListarCajeros.setBounds(10, 11, 140, 22);
		pnl_Listar_Manto.add(lblListarCajeros);

		JButton btnNewButton_1 = new JButton("Listar");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				listarCajero();
			}
		});
		btnNewButton_1.setBounds(173, 40, 89, 23);
		pnl_Listar_Manto.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 74, 450, 107);
		pnl_Listar_Manto.add(scrollPane);

		txtS_Cajeros = new JTextArea();
		scrollPane.setViewportView(txtS_Cajeros);

		txtCodCajero.setText(generarIDCajero() + "");

		lbl_estadoIngresar = new JLabel("");
		lbl_estadoIngresar.setForeground(Color.RED);
		lbl_estadoIngresar.setBounds(10, 168, 275, 14);
		pnl_Ingresar_Manto.add(lbl_estadoIngresar);

		setVisible(false);
	}

	public void grabarCajero() {
		String name, ape, fono, login, pwd;
		int turno = -1, id;
		id = Integer.parseInt(txtCodCajero.getText());
		name = txtName.getText();
		ape = txtApellido.getText();
		fono = txtFono.getText();
		login = txtLogin.getText();
		pwd = txtPwd.getText();
		switch (cboTurno.getSelectedIndex()) {
		case 0:
			turno = 0;
			break;
		case 1:
			turno = 1;
			break;
		}
		Cajero objC = new Cajero(id, ape, name, fono, login, pwd, turno);
		notificarIngreso("Cajero " + id + " registrado");
		aCajero.agregar(objC);
		aCajero.grabarCajero();
		limpiarPNL_ingresar();
	}

	public int generarIDCajero() {
		int canti = aCajero.tama�o();
		int id;
		if (canti == 0) {
			id = 1;
		} else {
			id = canti + 1;
		}
		return id;
	}

	public void buscarCajero_pnlModificar() {
		int codi = Integer.parseInt(txtCodCajero_Pmodificar.getText());
		Cajero objC = aCajero.buscar(codi);
		if (objC == null) {
			notificarModificacion("Cajero " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			txtApe_pnlModi.setText(objC.getApeCajero());
			txtName_pnlModi.setText(objC.getNameCajero());
			txtFono_pnlModi.setText(objC.getFonoCajero());
			txtLogin_pnlModi.setText(objC.getLoginCajero());
			txtPwd_pnlModi.setText(objC.getPwdCajero());
			switch (objC.getTurnoCajero()) {
			case 0:
				cboTurno_pnlModi.setSelectedIndex(0);
				break;
			case 1:
				cboTurno_pnlModi.setSelectedIndex(1);
				break;
			}
			notificarModificacion("Cajero " + codi + " encontrado");
		}
	}

	public void actualizarCajero() {
		int codi = Integer.parseInt(txtCodCajero_Pmodificar.getText());
		Cajero objC = aCajero.buscar(codi);
		if (objC == null) {
			notificarModificacion("Cajero " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			objC.setApeCajero(txtApe_pnlModi.getText());
			objC.setNameCajero(txtName_pnlModi.getText());
			objC.setFonoCajero(txtFono_pnlModi.getText());
			objC.setLoginCajero(txtLogin_pnlModi.getText());
			objC.setPwdCajero(txtPwd_pnlModi.getText());
			switch (cboTurno_pnlModi.getSelectedIndex()) {
			case 0:
				objC.setTurnoCajero(0);
				break;
			case 1:
				objC.setTurnoCajero(1);
				break;
			}
			aCajero.grabarCajero();
			notificarModificacion("Cajero " + codi + " actualizado");
		}
	}

	public void buscarCajero_pnlEliminar() {
		int codi = Integer.parseInt(txtCodCajero_pnlEliminar.getText());
		Cajero objC = aCajero.buscar(codi);
		if (objC == null) {
			notificarEliminar("Cajero " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			txtApeCajero_pnlEliminar.setText(objC.getApeCajero());
			txtName_pnlEliminar.setText(objC.getNameCajero());
			txtFonoCajero_pnlEliminar.setText(objC.getFonoCajero());
			txtLoginCajero_pnlEliminar.setText(objC.getLoginCajero());
			txtPwdCajero_pnlEliminar.setText(objC.getPwdCajero());
			txtTurnoCajero_pnlEliminar.setText(objC.getTurnoCajero() + "");
			notificarEliminar("Cajero " + codi + " encontrado");
		}
	}

	public void eliminarCajero() {
		int codi = Integer.parseInt(txtCodCajero_pnlEliminar.getText());
		Cajero objC = aCajero.buscar(codi);
		if (objC == null) {
			notificarEliminar("Cajero " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			aCajero.eliminar(objC);
			aCajero.grabarCajero();
			notificarEliminar("Cajero " + codi + " eliminado");
		}
	}

	public void buscarCajero_pnlConsultar() {
		int codi = Integer.parseInt(txtCodCajero_pnlConsultar.getText());
		Cajero objC = aCajero.buscar(codi);
		if (objC == null) {
			notificarConsultar("Cajero " + codi + " NO existe");
			limpiarPNL_consultar();
		} else {
			txtApeCajero_pnlConsultar.setText(objC.getApeCajero());
			txtNameCajero_pnlConsultar.setText(objC.getNameCajero());
			txtFonoCajero_pnlConsultar.setText(objC.getFonoCajero());
			txtLoginCajero_pnlConsultar.setText(objC.getLoginCajero());
			txtPwdCajero_pnlConsultar.setText(objC.getPwdCajero());
			txtTurnoCajero_pnlConsultar.setText(objC.getTurnoCajero() + "");
			notificarConsultar("Cajero " + codi + " encontrado");
		}
	}

	public void listarCajero() {
		String linea = "", turno = "";
		txtS_Cajeros.setText("");
		txtS_Cajeros.append("C�digo\tApellido \tNombre \tFono \tEstado\n");
		txtS_Cajeros
				.append("===========================================================\n");
		for (int i = 0; i < aCajero.tama�o(); i++) {
			switch (aCajero.obtener(i).getTurnoCajero()) {
			case 0:
				turno = "Noche - 0";
				break;
			case 1:
				turno = "Dia - 1";
				break;
			}
			linea = aCajero.obtener(i).getCodCajero() + "\t"
					+ aCajero.obtener(i).getApeCajero() + "\t"
					+ aCajero.obtener(i).getNameCajero() + "\t"
					+ aCajero.obtener(i).getFonoCajero() + "\t"
					+ aCajero.obtener(i).getLoginCajero() + "\t"
					+ aCajero.obtener(i).getPwdCajero() + "\t" + turno + "\n";
			txtS_Cajeros.append(linea);
		}
	}

	public void mensaje(String s) {
		JOptionPane.showMessageDialog(this, s);
	}

	public void notificarIngreso(String s) {
		lbl_estadoIngresar.setText(s);
	}

	public void notificarModificacion(String s) {
		lbl_estadoModificar.setText(s);
	}

	public void notificarEliminar(String s) {
		lbl_estadoEliminar.setText(s);
	}

	public void notificarConsultar(String s) {
		lbl_estadoConsultar.setText(s);
	}

	public void limpiarPNL_ingresar() {
		txtCodCajero.setText(generarIDCajero() + "");
		txtName.setText("");
		txtApellido.setText("");
		txtFono.setText("");
		txtLogin.setText("");
		txtPwd.setText("");
	}

	public void limpiarPNL_modificar() {
		txtCodCajero_Pmodificar.setText("");
		txtApe_pnlModi.setText("");
		txtName_pnlModi.setText("");
		txtFono_pnlModi.setText("");
		txtLogin_pnlModi.setText("");
		txtPwd_pnlModi.setText("");
	}

	public void limpiarPNL_eliminar() {
		txtCodCajero_pnlEliminar.setText("");
		txtApeCajero_pnlEliminar.setText("");
		txtName_pnlEliminar.setText("");
		txtFonoCajero_pnlEliminar.setText("");
		txtLoginCajero_pnlEliminar.setText("");
		txtPwdCajero_pnlEliminar.setText("");
		txtTurnoCajero_pnlEliminar.setText("");
	}

	public void limpiarPNL_consultar() {
		txtCodCajero_pnlConsultar.setText("");
		txtApeCajero_pnlConsultar.setText("");
		txtNameCajero_pnlConsultar.setText("");
		txtFonoCajero_pnlConsultar.setText("");
		txtLoginCajero_pnlConsultar.setText("");
		txtPwdCajero_pnlConsultar.setText("");
		txtTurnoCajero_pnlConsultar.setText("");
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == lblSalir_1_1_1) {
			demoGUI.habilita(DemoGUI.PANEL_1_1);
		}
		if (e.getSource() == btnRegistrar) {
			grabarCajero();
		}
	}
}
