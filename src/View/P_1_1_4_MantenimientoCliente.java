package View;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;
import java.awt.geom.Area;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import Controller.Cliente;
import Model.ArregloCliente;

public class P_1_1_4_MantenimientoCliente extends JPanel implements
		ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtNameCliente_pIngresar;
	private JTextField txtApeCliente_pIngresar;
	private JTextField txtFonoCliente_pIngresar;
	private JTextField txtCodCliente_pIngresar;
	private JTextField txtCodCliente_pEliminar;
	private JTextField txtCodCliente_pModificar;
	private JTextField txtCodCliente_pConsultar;
	private JTextField txtFonoCliente_pModificar;
	private JTextField txtNameCliente_pModificar;
	private JTextField txtApeCliente_pModificar;
	private JTextField txtFonoCliente_pEliminar;
	private JTextField txtNameCliente_pEliminar;
	private JTextField txtEstadoCliente_pEliminar;
	private JTextField txtApeCliente_pEliminar;
	private JTextField txtFonoCliente_pConsultar;
	private JTextField txtNameCliente_pConsultar;
	private JTextField txtEstadoCliente_pConsultar;
	private JTextField txtApeCliente_pConsultar;
	private JComboBox cboCliente_pIngresar;
	private JLabel lblNotificar_modificar;
	private JComboBox cboEstadoCliente_pModificar;
	private JLabel lblNotificar_ingresar;
	private JLabel lblNotificar_eliminar;

	public ArregloCliente aCliente = new ArregloCliente();
	private JLabel lblNotificar_consultar;
	private JTextArea taCliente;

	public P_1_1_4_MantenimientoCliente(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(589, 344);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Mantenimiento - Cliente", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel lblMantenimientoCajeros = new JLabel("Mantenimiento - Cliente");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		JLabel lblSalir_1_1_4 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_1_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		lblSalir_1_1_4.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_1_4.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_1_4.setBounds(10, 300, 70, 33);
		add(lblSalir_1_1_4);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 546, 231);
		add(tabbedPane);

		JPanel pnl_Ingresar_Manto = new JPanel();
		pnl_Ingresar_Manto.setToolTipText("");
		tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Manto, null);
		pnl_Ingresar_Manto.setLayout(null);

		JLabel lblIngresarCajeros = new JLabel("Ingresar Cliente");
		lblIngresarCajeros.setBounds(10, 11, 219, 22);
		lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarCajeros.setForeground(Color.BLUE);
		lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblIngresarCajeros.setAlignmentX(0.5f);
		pnl_Ingresar_Manto.add(lblIngresarCajeros);

		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(10, 57, 65, 14);
		pnl_Ingresar_Manto.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Apellido");
		lblNewLabel_1.setBounds(248, 55, 78, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Tel\u00E9fono");
		lblNewLabel_2.setBounds(10, 87, 65, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Estado");
		lblNewLabel_4.setBounds(248, 85, 78, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_4);

		txtNameCliente_pIngresar = new JTextField();
		txtNameCliente_pIngresar.setBounds(85, 51, 153, 20);
		pnl_Ingresar_Manto.add(txtNameCliente_pIngresar);
		txtNameCliente_pIngresar.setColumns(10);

		txtApeCliente_pIngresar = new JTextField();
		txtApeCliente_pIngresar.setBounds(345, 50, 185, 20);
		pnl_Ingresar_Manto.add(txtApeCliente_pIngresar);
		txtApeCliente_pIngresar.setColumns(10);

		txtFonoCliente_pIngresar = new JTextField();
		txtFonoCliente_pIngresar.setBounds(85, 81, 153, 20);
		pnl_Ingresar_Manto.add(txtFonoCliente_pIngresar);
		txtFonoCliente_pIngresar.setColumns(10);

		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registrarCliente();
			}
		});
		btnRegistrar.setBounds(365, 152, 165, 40);
		pnl_Ingresar_Manto.add(btnRegistrar);

		JLabel lblCdigo = new JLabel("C\u00F3digo");
		lblCdigo.setBounds(239, 18, 106, 14);
		pnl_Ingresar_Manto.add(lblCdigo);

		txtCodCliente_pIngresar = new JTextField();
		txtCodCliente_pIngresar.setEditable(false);
		txtCodCliente_pIngresar.setBounds(345, 15, 185, 20);
		pnl_Ingresar_Manto.add(txtCodCliente_pIngresar);
		txtCodCliente_pIngresar.setColumns(10);

		cboCliente_pIngresar = new JComboBox();
		cboCliente_pIngresar.setModel(new DefaultComboBoxModel(new String[] {
				"No activo", "Activo" }));
		cboCliente_pIngresar.setBounds(345, 84, 185, 20);
		pnl_Ingresar_Manto.add(cboCliente_pIngresar);

		lblNotificar_ingresar = new JLabel("");
		lblNotificar_ingresar.setForeground(Color.RED);
		lblNotificar_ingresar.setBounds(10, 178, 345, 14);
		pnl_Ingresar_Manto.add(lblNotificar_ingresar);

		JPanel pnl_Modificar_Manto = new JPanel();
		tabbedPane.addTab("Modificar", null, pnl_Modificar_Manto, null);
		pnl_Modificar_Manto.setLayout(null);

		JLabel lblModificarCajeros = new JLabel("Modificar Cliente");
		lblModificarCajeros.setBounds(10, 11, 218, 22);
		lblModificarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblModificarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblModificarCajeros.setForeground(Color.BLUE);
		lblModificarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblModificarCajeros.setAlignmentX(0.5f);
		pnl_Modificar_Manto.add(lblModificarCajeros);

		JButton btnBuscar_pModificar = new JButton("b");
		btnBuscar_pModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarCLiente_pModificar();
			}
		});
		btnBuscar_pModificar.setBounds(491, 11, 39, 26);
		pnl_Modificar_Manto.add(btnBuscar_pModificar);

		txtCodCliente_pModificar = new JTextField();
		txtCodCliente_pModificar.setColumns(10);
		txtCodCliente_pModificar.setBounds(345, 11, 128, 20);
		pnl_Modificar_Manto.add(txtCodCliente_pModificar);

		JLabel lblCdigo_1 = new JLabel("C\u00F3digo Cliente");
		lblCdigo_1.setBounds(236, 16, 99, 14);
		pnl_Modificar_Manto.add(lblCdigo_1);

		JButton btnActualizarCliente = new JButton("Grabar");
		btnActualizarCliente.setBounds(365, 152, 165, 40);
		pnl_Modificar_Manto.add(btnActualizarCliente);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 44, 541, 9);
		pnl_Modificar_Manto.add(separator);

		txtFonoCliente_pModificar = new JTextField();
		txtFonoCliente_pModificar.setColumns(10);
		txtFonoCliente_pModificar.setBounds(84, 95, 153, 20);
		pnl_Modificar_Manto.add(txtFonoCliente_pModificar);

		txtNameCliente_pModificar = new JTextField();
		txtNameCliente_pModificar.setColumns(10);
		txtNameCliente_pModificar.setBounds(84, 65, 153, 20);
		pnl_Modificar_Manto.add(txtNameCliente_pModificar);

		txtApeCliente_pModificar = new JTextField();
		txtApeCliente_pModificar.setColumns(10);
		txtApeCliente_pModificar.setBounds(345, 65, 185, 20);
		pnl_Modificar_Manto.add(txtApeCliente_pModificar);

		JLabel label_1 = new JLabel("Nombre");
		label_1.setBounds(10, 68, 64, 14);
		pnl_Modificar_Manto.add(label_1);

		JLabel label_2 = new JLabel("Tel\u00E9fono");
		label_2.setBounds(10, 98, 64, 14);
		pnl_Modificar_Manto.add(label_2);

		JLabel label_3 = new JLabel("Estado");
		label_3.setBounds(261, 98, 74, 14);
		pnl_Modificar_Manto.add(label_3);

		JLabel label_4 = new JLabel("Apellido");
		label_4.setBounds(261, 68, 74, 14);
		pnl_Modificar_Manto.add(label_4);

		cboEstadoCliente_pModificar = new JComboBox();
		cboEstadoCliente_pModificar.setModel(new DefaultComboBoxModel(
				new String[] { "No activo", "Activo" }));
		cboEstadoCliente_pModificar.setBounds(345, 95, 185, 20);
		pnl_Modificar_Manto.add(cboEstadoCliente_pModificar);

		lblNotificar_modificar = new JLabel("");
		lblNotificar_modificar.setForeground(Color.RED);
		lblNotificar_modificar.setBounds(10, 178, 325, 14);
		pnl_Modificar_Manto.add(lblNotificar_modificar);

		JPanel pnl_Eliminar_Manto = new JPanel();
		tabbedPane.addTab("Eliminar", null, pnl_Eliminar_Manto, null);
		pnl_Eliminar_Manto.setLayout(null);

		JLabel lblEliminarCajeros = new JLabel("Eliminar Cliente");
		lblEliminarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEliminarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblEliminarCajeros.setForeground(Color.BLUE);
		lblEliminarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblEliminarCajeros.setAlignmentX(0.5f);
		lblEliminarCajeros.setBounds(10, 11, 209, 22);
		pnl_Eliminar_Manto.add(lblEliminarCajeros);

		txtCodCliente_pEliminar = new JTextField();
		txtCodCliente_pEliminar.setColumns(10);
		txtCodCliente_pEliminar.setBounds(345, 15, 116, 20);
		pnl_Eliminar_Manto.add(txtCodCliente_pEliminar);

		JButton btnBuscarCliente_pEliminar = new JButton("b");
		btnBuscarCliente_pEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarCLiente_pEliminar();
			}
		});
		btnBuscarCliente_pEliminar.setBounds(491, 11, 39, 26);
		pnl_Eliminar_Manto.add(btnBuscarCliente_pEliminar);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 43, 541, 9);
		pnl_Eliminar_Manto.add(separator_1);

		JLabel lblCdigoMedicamento = new JLabel("C\u00F3digo cliente");
		lblCdigoMedicamento.setBounds(229, 16, 106, 14);
		pnl_Eliminar_Manto.add(lblCdigoMedicamento);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(365, 152, 165, 40);
		pnl_Eliminar_Manto.add(btnEliminar);

		txtFonoCliente_pEliminar = new JTextField();
		txtFonoCliente_pEliminar.setEditable(false);
		txtFonoCliente_pEliminar.setColumns(10);
		txtFonoCliente_pEliminar.setBounds(66, 96, 153, 20);
		pnl_Eliminar_Manto.add(txtFonoCliente_pEliminar);

		txtNameCliente_pEliminar = new JTextField();
		txtNameCliente_pEliminar.setEditable(false);
		txtNameCliente_pEliminar.setColumns(10);
		txtNameCliente_pEliminar.setBounds(66, 66, 153, 20);
		pnl_Eliminar_Manto.add(txtNameCliente_pEliminar);

		txtEstadoCliente_pEliminar = new JTextField();
		txtEstadoCliente_pEliminar.setEditable(false);
		txtEstadoCliente_pEliminar.setColumns(10);
		txtEstadoCliente_pEliminar.setBounds(345, 94, 185, 20);
		pnl_Eliminar_Manto.add(txtEstadoCliente_pEliminar);

		txtApeCliente_pEliminar = new JTextField();
		txtApeCliente_pEliminar.setEditable(false);
		txtApeCliente_pEliminar.setColumns(10);
		txtApeCliente_pEliminar.setBounds(345, 63, 185, 20);
		pnl_Eliminar_Manto.add(txtApeCliente_pEliminar);

		JLabel label_5 = new JLabel("Nombre");
		label_5.setBounds(10, 63, 46, 14);
		pnl_Eliminar_Manto.add(label_5);

		JLabel label_6 = new JLabel("Tel\u00E9fono");
		label_6.setBounds(10, 93, 46, 14);
		pnl_Eliminar_Manto.add(label_6);

		JLabel label_7 = new JLabel("Estado");
		label_7.setBounds(229, 93, 106, 14);
		pnl_Eliminar_Manto.add(label_7);

		JLabel label_8 = new JLabel("Apellido");
		label_8.setBounds(229, 63, 106, 14);
		pnl_Eliminar_Manto.add(label_8);

		lblNotificar_eliminar = new JLabel("");
		lblNotificar_eliminar.setForeground(Color.RED);
		lblNotificar_eliminar.setBounds(10, 178, 325, 14);
		pnl_Eliminar_Manto.add(lblNotificar_eliminar);

		JPanel pnl_Consultar_Manto = new JPanel();
		tabbedPane.addTab("Consultar", null, pnl_Consultar_Manto, null);
		pnl_Consultar_Manto.setLayout(null);

		JLabel lblConsultarCajeros = new JLabel("Consultar Cliente");
		lblConsultarCajeros.setBounds(10, 11, 216, 22);
		lblConsultarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblConsultarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsultarCajeros.setForeground(Color.BLUE);
		lblConsultarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblConsultarCajeros.setAlignmentX(0.5f);
		pnl_Consultar_Manto.add(lblConsultarCajeros);

		JButton btnBuscar_pConsultar = new JButton("b");
		btnBuscar_pConsultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				consultarCliente();
			}
		});
		btnBuscar_pConsultar.setBounds(491, 11, 39, 26);
		pnl_Consultar_Manto.add(btnBuscar_pConsultar);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 42, 541, 9);
		pnl_Consultar_Manto.add(separator_2);

		JLabel lblCdigoMedicamento_1 = new JLabel("C\u00F3digo cliente");
		lblCdigoMedicamento_1.setBounds(236, 12, 99, 14);
		pnl_Consultar_Manto.add(lblCdigoMedicamento_1);

		txtCodCliente_pConsultar = new JTextField();
		txtCodCliente_pConsultar.setColumns(10);
		txtCodCliente_pConsultar.setBounds(345, 11, 116, 20);
		pnl_Consultar_Manto.add(txtCodCliente_pConsultar);

		txtFonoCliente_pConsultar = new JTextField();
		txtFonoCliente_pConsultar.setEditable(false);
		txtFonoCliente_pConsultar.setColumns(10);
		txtFonoCliente_pConsultar.setBounds(66, 111, 153, 20);
		pnl_Consultar_Manto.add(txtFonoCliente_pConsultar);

		txtNameCliente_pConsultar = new JTextField();
		txtNameCliente_pConsultar.setEditable(false);
		txtNameCliente_pConsultar.setColumns(10);
		txtNameCliente_pConsultar.setBounds(66, 81, 153, 20);
		pnl_Consultar_Manto.add(txtNameCliente_pConsultar);

		txtEstadoCliente_pConsultar = new JTextField();
		txtEstadoCliente_pConsultar.setEditable(false);
		txtEstadoCliente_pConsultar.setColumns(10);
		txtEstadoCliente_pConsultar.setBounds(345, 109, 185, 20);
		pnl_Consultar_Manto.add(txtEstadoCliente_pConsultar);

		txtApeCliente_pConsultar = new JTextField();
		txtApeCliente_pConsultar.setEditable(false);
		txtApeCliente_pConsultar.setColumns(10);
		txtApeCliente_pConsultar.setBounds(345, 78, 185, 20);
		pnl_Consultar_Manto.add(txtApeCliente_pConsultar);

		JLabel label = new JLabel("Nombre");
		label.setBounds(10, 87, 46, 14);
		pnl_Consultar_Manto.add(label);

		JLabel label_9 = new JLabel("Tel\u00E9fono");
		label_9.setBounds(10, 117, 46, 14);
		pnl_Consultar_Manto.add(label_9);

		JLabel label_10 = new JLabel("Estado");
		label_10.setBounds(229, 117, 106, 14);
		pnl_Consultar_Manto.add(label_10);

		JLabel label_11 = new JLabel("Apellido");
		label_11.setBounds(229, 87, 106, 14);
		pnl_Consultar_Manto.add(label_11);

		lblNotificar_consultar = new JLabel("");
		lblNotificar_consultar.setForeground(Color.RED);
		lblNotificar_consultar.setBounds(10, 178, 325, 14);
		pnl_Consultar_Manto.add(lblNotificar_consultar);

		JPanel pnl_Listar_Manto = new JPanel();
		tabbedPane.addTab("Listar", null, pnl_Listar_Manto, null);
		pnl_Listar_Manto.setLayout(null);

		JLabel lblListarCajeros = new JLabel("Listar Cliente");
		lblListarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblListarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarCajeros.setForeground(Color.BLUE);
		lblListarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarCajeros.setAlignmentX(0.5f);
		lblListarCajeros.setBounds(10, 11, 201, 22);
		pnl_Listar_Manto.add(lblListarCajeros);

		JButton btnListar_pListar = new JButton("Listar");
		btnListar_pListar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				listarCliente();
			}
		});
		btnListar_pListar.setBounds(173, 40, 89, 23);
		pnl_Listar_Manto.add(btnListar_pListar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 74, 521, 118);
		pnl_Listar_Manto.add(scrollPane);

		taCliente = new JTextArea();
		taCliente.setEditable(false);
		scrollPane.setViewportView(taCliente);

		txtCodCliente_pIngresar.setText(generarIDCliente() + "");

		setVisible(false);
	}

	public void registrarCliente() {
		String ape, name, fono;
		int estado = -1, cod;
		cod = Integer.parseInt(txtCodCliente_pIngresar.getText());
		name = txtNameCliente_pIngresar.getText();
		ape = txtApeCliente_pIngresar.getText();
		fono = txtFonoCliente_pIngresar.getText();
		switch (cboCliente_pIngresar.getSelectedIndex()) {
		case 0:
			estado = 0;
			break;
		case 1:
			estado = 1;
			break;
		}
		Cliente objC = new Cliente(cod, ape, name, fono, estado);
		aCliente.agregar(objC);
		aCliente.grabarCliente();
		limpiarPNL_ingresar();
		notificarIngresar("Cliente " + cod + "registrado");
	}

	public void buscarCLiente_pModificar() {
		int codi = Integer.parseInt(txtCodCliente_pModificar.getText());
		Cliente objC = aCliente.buscar(codi);
		if (objC == null) {
			notificarModificar("Cliente " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			txtApeCliente_pModificar.setText(objC.getApeCliente());
			txtNameCliente_pModificar.setText(objC.getNameCliente());
			txtFonoCliente_pModificar.setText(objC.getFonoCliente());
			switch (objC.getEstado()) {
			case 0:
				cboEstadoCliente_pModificar.setSelectedIndex(0);
				break;
			case 1:
				cboEstadoCliente_pModificar.setSelectedIndex(1);
				break;
			}
			notificarModificar("Cliente " + codi + " encontrado");
		}
	}

	public void actualizarCliente() {
		int codi = Integer.parseInt(txtCodCliente_pModificar.getText());
		Cliente objC = aCliente.buscar(codi);
		if (objC == null) {
			notificarModificar("Cliente " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			objC.setApeCliente(txtApeCliente_pIngresar.getText());
			objC.setNameCliente(txtNameCliente_pIngresar.getText());
			objC.setFonoCliente(txtFonoCliente_pIngresar.getText());
			switch (cboEstadoCliente_pModificar.getSelectedIndex()) {
			case 0:
				objC.setEstado(0);
				break;
			case 1:
				objC.setEstado(1);
				break;
			}
			aCliente.grabarCliente();
			notificarModificar("Cliente " + codi + " actualizado");
		}
	}

	public void buscarCLiente_pEliminar() {
		int codi = Integer.parseInt(txtCodCliente_pEliminar.getText());
		Cliente objC = aCliente.buscar(codi);
		if (objC == null) {
			notificarEliminar("Cliente " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			txtApeCliente_pEliminar.setText(objC.getApeCliente());
			txtNameCliente_pEliminar.setText(objC.getNameCliente());
			txtFonoCliente_pEliminar.setText(objC.getFonoCliente());
			switch (objC.getEstado()) {
			case 0:
				txtEstadoCliente_pEliminar.setText(objC.getEstado()
						+ " - No activo");
				break;
			case 1:
				txtEstadoCliente_pEliminar.setText(objC.getEstado()
						+ " - Activo");
				break;
			}
			notificarEliminar("Cliente " + codi + " encontrado");
		}
	}

	public void eliminarCliente() {
		int codi = Integer.parseInt(txtCodCliente_pEliminar.getText());
		Cliente objC = aCliente.buscar(codi);
		if (objC == null) {
			notificarEliminar("Cliente " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			aCliente.eliminar(objC);
			aCliente.grabarCliente();
			notificarEliminar("Cliente " + codi + " eliminado");
		}
	}

	public void consultarCliente() {
		int codi = Integer.parseInt(txtCodCliente_pConsultar.getText());
		Cliente objC = aCliente.buscar(codi);
		if (objC == null) {
			notificarConsultar("Cliente " + codi + " NO existe");
			limpiarPNL_consultar();
		} else {
			txtApeCliente_pConsultar.setText(objC.getApeCliente());
			txtNameCliente_pConsultar.setText(objC.getNameCliente());
			txtFonoCliente_pConsultar.setText(objC.getFonoCliente());
			switch (objC.getEstado()) {
			case 0:
				txtEstadoCliente_pConsultar.setText(objC.getEstado()
						+ " - No activo");
				break;
			case 1:
				txtEstadoCliente_pConsultar.setText(objC.getEstado()
						+ " - Activo");
				break;
			}
			notificarConsultar("Cliente " + codi + " encontrado");
		}
	}

	public void listarCliente() {
		String linea = "", estado = "";
		taCliente.setText("");
		taCliente.append("C�digo\tApellido \tNombre \tFono \tEstado\n");
		taCliente
				.append("===========================================================\n");
		for (int i = 0; i < aCliente.tama�o(); i++) {
			switch (aCliente.obtener(i).getEstado()) {
			case 0:
				estado = "No activo - 0";
				break;
			case 1:
				estado = "Activo - 1";
				break;
			}
			linea = aCliente.obtener(i).getCodCliente() + "\t"
					+ aCliente.obtener(i).getApeCliente() + "\t"
					+ aCliente.obtener(i).getNameCliente() + "\t"
					+ aCliente.obtener(i).getFonoCliente() + "\t" + estado
					+ "\n";
			taCliente.append(linea);
		}

	}

	public void limpiarPNL_ingresar() {
		txtCodCliente_pIngresar.setText(generarIDCliente() + "");
		txtApeCliente_pIngresar.setText("");
		txtNameCliente_pIngresar.setText("");
		txtFonoCliente_pIngresar.setText("");
		cboEstadoCliente_pModificar.setSelectedIndex(-1);
	}

	public void limpiarPNL_modificar() {
		txtCodCliente_pModificar.setText("");
		txtApeCliente_pModificar.setText("");
		txtNameCliente_pModificar.setText("");
		txtFonoCliente_pModificar.setText("");
		cboEstadoCliente_pModificar.setSelectedIndex(-1);
	}

	public void limpiarPNL_eliminar() {
		txtCodCliente_pEliminar.setText("");
		txtApeCliente_pEliminar.setText("");
		txtNameCliente_pEliminar.setText("");
		txtFonoCliente_pEliminar.setText("");
		txtEstadoCliente_pEliminar.setText("");
	}

	public void limpiarPNL_consultar() {
		txtApeCliente_pConsultar.setText("");
		txtCodCliente_pConsultar.setText("");
		txtEstadoCliente_pConsultar.setText("");
		txtFonoCliente_pConsultar.setText("");
		txtNameCliente_pConsultar.setText("");
	}

	public int generarIDCliente() {
		int canti = aCliente.tama�o();
		int id;
		if (canti == 0) {
			id = 1;
		} else {
			id = canti + 1;
		}
		return id;
	}

	public void notificarIngresar(String s) {
		lblNotificar_ingresar.setText(s);
	}

	public void notificarModificar(String s) {
		lblNotificar_modificar.setText(s);
	}

	public void notificarEliminar(String s) {
		lblNotificar_eliminar.setText(s);
	}

	public void notificarConsultar(String s) {
		lblNotificar_consultar.setText(s);
	}

	public void actionPerformed(ActionEvent e) {
	}
}
