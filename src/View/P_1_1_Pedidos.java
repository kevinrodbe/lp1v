package View;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.DefaultTableModel;

import java.awt.event.*;

public class P_1_1_Pedidos extends JPanel implements ActionListener {
	private DemoGUI demoGUI;
	public JButton btnPdf;
	private JLabel lblSalir_1;
	private JTable table;

	public P_1_1_Pedidos(final DemoGUI demoGUI) {
		setFont(new Font("Tahoma", Font.BOLD, 14));
		this.demoGUI = demoGUI;

		setSize(572, 380);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Mantenimiento", TitledBorder.LEADING, TitledBorder.TOP, null,
				Color.BLACK));

		JLabel lblNewLabel = new JLabel("Mantenimiento");
		lblNewLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(220, 29, 144, 25);
		add(lblNewLabel);

		btnPdf = new JButton("Exportar PDF");
		btnPdf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_1_4);
			}
		});
		btnPdf.setBounds(438, 319, 123, 50);
		add(btnPdf);

		lblSalir_1 = new JLabel("<< Atr\u00E1s");
		lblSalir_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1);
			}
		});
		lblSalir_1.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(0,
				0, 0)));
		lblSalir_1.setBounds(10, 336, 70, 33);
		add(lblSalir_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 59, 551, 249);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"Descripción", "Precio", "Stock", "Marca" }) {
			/**
					 * 
					 */
					private static final long serialVersionUID = 1L;
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] { String.class, String.class,
					String.class, String.class };

			@SuppressWarnings("rawtypes")
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		setVisible(false);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnPdf) {
			demoGUI.habilita(DemoGUI.PANEL_1_1_4);
		}
		if (e.getSource() == lblSalir_1) {
			demoGUI.habilita(DemoGUI.PANEL_1);
		}
	}
}
