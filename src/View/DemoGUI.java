package View;

import java.awt.Dimension;

import javax.swing.JFrame;

public class DemoGUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private P_1_Principal_logistica pnl_1;
	private P_1_1_Pedidos pnl_1_1;
	private P_1_1_1_MantenimientoCajero pnl_1_1_1;
	private P_1_1_2_MantenimientoMascota pnl_1_1_2;
	private P_1_1_3_MantenimientoMedicamento pnl_1_1_3;
	private P_1_1_4_MantenimientoCliente pnl_1_1_4;
	private P_1_2_cuadro pnl_1_2;
	private P_1_3_empresa pnl_1_3;
	private P_1_4_MenuReporte pnl_1_4;

	// constantes
	public static final int PANEL_1 = 1;
	public static final int PANEL_1_1 = 2;
	public static final int PANEL_1_1_1 = 3;
	public static final int PANEL_1_1_2 = 4;
	public static final int PANEL_1_1_3 = 5;
	public static final int PANEL_1_1_4 = 6;
	public static final int PANEL_1_2 = 7;
	public static final int PANEL_1_3 = 8;
	public static final int PANEL_1_4 = 9;

	public DemoGUI() {

		getContentPane().setLayout(null);
		pack();

		pnl_1 = new P_1_Principal_logistica(this);
		pnl_1.setLocation(0, 0);
		pnl_1.setSize(300, 380);
		pnl_1.setPreferredSize(new Dimension(300, 380));
		pnl_1.setBounds(0, 0, 300, 380);
		getContentPane().add(pnl_1);
		pack();

		pnl_1_1 = new P_1_1_Pedidos(this);
		pnl_1_1.setLocation(0, 0);
		getContentPane().add(pnl_1_1);

		pnl_1_1_1 = new P_1_1_1_MantenimientoCajero(this);
		pnl_1_1_1.setLocation(0, 0);
		getContentPane().add(pnl_1_1_1);

		pnl_1_1_2 = new P_1_1_2_MantenimientoMascota(this);
		pnl_1_1_2.setLocation(0, 0);
		getContentPane().add(pnl_1_1_2);

		pnl_1_1_3 = new P_1_1_3_MantenimientoMedicamento(this);
		pnl_1_1_3.setLocation(0, 0);
		getContentPane().add(pnl_1_1_3);

		pnl_1_1_4 = new P_1_1_4_MantenimientoCliente(this);
		pnl_1_1_4.setLocation(0, 0);
		getContentPane().add(pnl_1_1_4);

		pnl_1_2 = new P_1_2_cuadro(this);
		pnl_1_2.setLocation(0, 0);
		getContentPane().add(pnl_1_2);

		pnl_1_3 = new P_1_3_empresa(this);
		pnl_1_3.setLocation(0, 0);
		getContentPane().add(pnl_1_3);

		pnl_1_4 = new P_1_4_MenuReporte(this);
		pnl_1_4.setLocation(0, 0);
		getContentPane().add(pnl_1_4);

		habilita(DemoGUI.PANEL_1);

		// setSize(600, 900);
	}

	public void habilita(int i) {
		desabilita();
		switch (i) {
		case PANEL_1:
			pnl_1.setVisible(true);
			setSize(315, 420);
			break;
		case PANEL_1_1:
			pnl_1_1.setVisible(true);
			setSize(580, 420);
			break;
		case PANEL_1_1_1:
			pnl_1_1_1.setVisible(true);
			setSize(511, 375);
			break;
		case PANEL_1_1_2:
			pnl_1_1_2.setVisible(true);
			setSize(608, 384);
			break;
		case PANEL_1_1_3:
			pnl_1_1_3.setVisible(true);
			setSize(608, 384);
			break;
		case PANEL_1_1_4:
			pnl_1_1_4.setVisible(true);
			setSize(608, 385);
			break;
		case PANEL_1_2:
			pnl_1_2.setVisible(true);
			setSize(610, 375);
			break;
		case PANEL_1_3:
			pnl_1_3.setVisible(true);
			setSize(400, 370);
			break;
		case PANEL_1_4:
			pnl_1_4.setVisible(true);
			setSize(420, 422);
			break;
		}
	}

	public void desabilita() {
		pnl_1.setVisible(false);
		pnl_1_1.setVisible(false);
		pnl_1_1_1.setVisible(false);
		pnl_1_1_2.setVisible(false);
		pnl_1_1_3.setVisible(false);
		pnl_1_1_4.setVisible(false);
		pnl_1_2.setVisible(false);
		pnl_1_3.setVisible(false);
		pnl_1_4.setVisible(false);
	}

	public static void main(String[] args) {
		DemoGUI frmInicial = new DemoGUI();
		frmInicial.setVisible(true);
	}
}
