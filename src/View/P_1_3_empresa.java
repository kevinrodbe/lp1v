package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;
import Controller.*;
import Model.*;

public class P_1_3_empresa extends JPanel implements ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtDiagnostico_pIngresar;
	private JTextField txtFechaReg_pIngresar;

	public ArregloMascota aMascota = new ArregloMascota();
	public ArregloCliente aCliente = new ArregloCliente();
	public ArregloCajero aCajero = new ArregloCajero();
	public ArregloCita aCita = new ArregloCita();
	public ArregloAtencion aAtencion = new ArregloAtencion();
	private JLabel lblNotificar_pIngresar;

	public P_1_3_empresa(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(375, 323);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Empresa", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel lblMantenimientoCajeros = new JLabel(
				"Empresas");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		JLabel lblSalir_1_3 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1);
			}
		});
		lblSalir_1_3.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_3.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_3.setBounds(10, 284, 70, 33);
		add(lblSalir_1_3);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 352, 215);
		add(tabbedPane);
		
				JPanel pnl_Ingresar_Manto = new JPanel();
				pnl_Ingresar_Manto.setToolTipText("");
				tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Manto, null);
				pnl_Ingresar_Manto.setLayout(null);
				
						JLabel lblIngresarCajeros = new JLabel("Ingresar Empresa");
						lblIngresarCajeros.setBounds(10, 11, 171, 22);
						lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
						lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
						lblIngresarCajeros.setForeground(Color.BLUE);
						lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
						lblIngresarCajeros.setAlignmentX(0.5f);
						pnl_Ingresar_Manto.add(lblIngresarCajeros);
						
								JButton btnRegistrar = new JButton("Registrar");
								btnRegistrar.addMouseListener(new MouseAdapter() {
									@Override
									public void mouseClicked(MouseEvent e) {
										registrarAtencion();
									}
								});
								btnRegistrar.setBounds(170, 110, 153, 40);
								pnl_Ingresar_Manto.add(btnRegistrar);
								
										JLabel lblDiagnstico = new JLabel("RUC");
										lblDiagnstico.setBounds(20, 47, 85, 14);
										pnl_Ingresar_Manto.add(lblDiagnstico);
										
												txtDiagnostico_pIngresar = new JTextField();
												txtDiagnostico_pIngresar.setColumns(10);
												txtDiagnostico_pIngresar.setBounds(170, 44, 153, 20);
												pnl_Ingresar_Manto.add(txtDiagnostico_pIngresar);
												
														JLabel lblFecRegistro = new JLabel("Nombre de empresa");
														lblFecRegistro.setBounds(20, 82, 140, 14);
														pnl_Ingresar_Manto.add(lblFecRegistro);
														
																txtFechaReg_pIngresar = new JTextField();
																txtFechaReg_pIngresar.setColumns(10);
																txtFechaReg_pIngresar.setBounds(170, 79, 153, 20);
																pnl_Ingresar_Manto.add(txtFechaReg_pIngresar);
																
																		lblNotificar_pIngresar = new JLabel("");
																		lblNotificar_pIngresar.setForeground(Color.RED);
																		lblNotificar_pIngresar.setBounds(10, 161, 313, 14);
																		pnl_Ingresar_Manto.add(lblNotificar_pIngresar);
		setVisible(false);
	}

	public void registrarAtencion() {
	}

	public void limpiarPNL_ingresar() {
		txtDiagnostico_pIngresar.setText("");
		txtFechaReg_pIngresar.setText("");
	}

	public void limpiarPNL_eliminar() {
	}

	public void notificarIngresar(String s) {
		lblNotificar_pIngresar.setText(s);
	}

	public void actionPerformed(ActionEvent e) {
	}
}
