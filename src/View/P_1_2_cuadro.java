package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;
import Controller.Cliente;
import Controller.Cajero;
import Controller.Mascota;
import Controller.Cita;
import Model.ArregloCliente;
import Model.ArregloCajero;
import Model.ArregloMascota;
import Model.ArregloCita;

public class P_1_2_cuadro extends JPanel implements ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtFechaIngreso_pCuadro;
	private JTextField txtFechaEntrega_pCuadro;
	private JTextField txtNroCuadro_pCuadro;
	private JComboBox cboRuc;
	private JComboBox cboNroPedido;
	private JLabel lblNotificar_pIngresar;

	public ArregloMascota aMascota = new ArregloMascota();
	public ArregloCliente aCliente = new ArregloCliente();
	public ArregloCajero aCajero = new ArregloCajero();
	public ArregloCita aCita = new ArregloCita();
	private JTextField txtMonto;
	private JTable table;

	public P_1_2_cuadro(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(589, 335);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cuadro de necesidades", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel lblMantenimientoCajeros = new JLabel("Cuadro de necesidades");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		JLabel lblSalir_1_2 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1);
			}
		});
		lblSalir_1_2.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_2.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_2.setBounds(10, 289, 70, 33);
		add(lblSalir_1_2);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 568, 220);
		add(tabbedPane);

		JPanel pnl_Ingresar_Cita = new JPanel();
		pnl_Ingresar_Cita.setToolTipText("");
		tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Cita, null);
		pnl_Ingresar_Cita.setLayout(null);

		JLabel lblIngresarCajeros = new JLabel("Ingresar Cuadro de necesidad");
		lblIngresarCajeros.setBounds(10, 11, 313, 22);
		lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarCajeros.setForeground(Color.BLUE);
		lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblIngresarCajeros.setAlignmentX(0.5f);
		pnl_Ingresar_Cita.add(lblIngresarCajeros);

		JLabel lblNewLabel = new JLabel("Nro Pedido");
		lblNewLabel.setBounds(20, 44, 85, 14);
		pnl_Ingresar_Cita.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("RUC Empresa");
		lblNewLabel_2.setBounds(20, 74, 85, 14);
		pnl_Ingresar_Cita.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Fec. Entrega");
		lblNewLabel_3.setBounds(295, 74, 83, 14);
		pnl_Ingresar_Cita.add(lblNewLabel_3);

		JLabel lblPassword = new JLabel("Fec. registro");
		lblPassword.setBounds(20, 104, 85, 14);
		pnl_Ingresar_Cita.add(lblPassword);

		JLabel lblNewLabel_4 = new JLabel("Monto");
		lblNewLabel_4.setBounds(295, 44, 83, 14);
		pnl_Ingresar_Cita.add(lblNewLabel_4);

		txtFechaIngreso_pCuadro = new JTextField();
		txtFechaIngreso_pCuadro.setColumns(10);
		txtFechaIngreso_pCuadro.setBounds(115, 100, 153, 20);
		pnl_Ingresar_Cita.add(txtFechaIngreso_pCuadro);

		txtFechaEntrega_pCuadro = new JTextField();
		txtFechaEntrega_pCuadro.setColumns(10);
		txtFechaEntrega_pCuadro.setBounds(388, 71, 165, 20);
		pnl_Ingresar_Cita.add(txtFechaEntrega_pCuadro);

		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registrarCita();
			}
		});
		btnRegistrar.setBounds(388, 141, 165, 40);
		pnl_Ingresar_Cita.add(btnRegistrar);

		JLabel lblCdigo = new JLabel("Nro Cuadro");
		lblCdigo.setBounds(366, 18, 77, 14);
		pnl_Ingresar_Cita.add(lblCdigo);

		txtNroCuadro_pCuadro = new JTextField();
		txtNroCuadro_pCuadro.setEditable(false);
		txtNroCuadro_pCuadro.setBounds(453, 15, 100, 20);
		pnl_Ingresar_Cita.add(txtNroCuadro_pCuadro);
		txtNroCuadro_pCuadro.setColumns(10);

		cboNroPedido = new JComboBox();
		cboNroPedido.setBounds(115, 41, 153, 20);
		pnl_Ingresar_Cita.add(cboNroPedido);

		cboRuc = new JComboBox();
		cboRuc.setBounds(115, 71, 153, 20);
		pnl_Ingresar_Cita.add(cboRuc);

		lblNotificar_pIngresar = new JLabel("");
		lblNotificar_pIngresar.setForeground(Color.RED);
		lblNotificar_pIngresar.setBounds(10, 167, 368, 14);
		pnl_Ingresar_Cita.add(lblNotificar_pIngresar);

		JPanel pnl_Listar_Manto = new JPanel();
		tabbedPane.addTab("Listar", null, pnl_Listar_Manto, null);
		pnl_Listar_Manto.setLayout(null);

		JLabel lblListarCajeros = new JLabel("Listar Cuadros");
		lblListarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblListarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarCajeros.setForeground(Color.BLUE);
		lblListarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarCajeros.setAlignmentX(0.5f);
		lblListarCajeros.setBounds(10, 11, 140, 22);
		pnl_Listar_Manto.add(lblListarCajeros);

		JButton btnListar = new JButton("Listar");
		btnListar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				listarCita();
			}
		});
		btnListar.setBounds(464, 14, 89, 23);
		pnl_Listar_Manto.add(btnListar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 44, 543, 137);
		pnl_Listar_Manto.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"Nro Cuadro", "Nro Pedido", "RUC", "Monto","Fecha Elaborada","Fecha. Entrega" }) {
			Class[] columnTypes = new Class[] { String.class, String.class,
					String.class, String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.setBorder(new LineBorder(new Color(0, 0, 0)));

		for (int i = 0; i < aCliente.tama�o(); i++) {
			cboNroPedido.addItem(aCliente.obtener(i).getCodCliente());
		}

		for (int i = 0; i < aCajero.tama�o(); i++) {
			cboRuc.addItem(aCajero.obtener(i).getCodCajero());
		}

		txtNroCuadro_pCuadro.setText("");
		
		txtMonto = new JTextField();
		txtMonto.setColumns(10);
		txtMonto.setBounds(388, 41, 165, 20);
		pnl_Ingresar_Cita.add(txtMonto);

		setVisible(false);
	}

	public void registrarCita() {
		String fREg, fAten;
		int codMascota, codCliente, estado = -1, codCajero, codCita;
		codCita = Integer.parseInt(txtNroCuadro_pCuadro.getText());
		codCliente = Integer.parseInt(cboNroPedido.getSelectedItem()
				.toString());
		codCajero = Integer.parseInt(cboRuc.getSelectedItem()
				.toString());
		codMascota = 2;
		fREg = txtFechaIngreso_pCuadro.getText();
		fAten = txtFechaEntrega_pCuadro.getText();
		Cita objC = new Cita(codCita, codCliente, codCajero, codMascota, fREg,
				fAten, estado);
		aCita.agregar(objC);
		aCita.grabarCita();
		limpiarPNL_ingresar();
		notificarIngresar("Cita " + codCita + " registrada");
	}

	public void listarCita() {
	}

	public void limpiarPNL_ingresar() {
		txtNroCuadro_pCuadro.setText( "");
		txtFechaIngreso_pCuadro.setText("");
		txtFechaEntrega_pCuadro.setText("");
		cboNroPedido.setSelectedIndex(-1);
		cboRuc.setSelectedIndex(-1);
	}

	public void limpiarPNL_eliminar() {
	}

	public void notificarIngresar(String s) {
		lblNotificar_pIngresar.setText(s);
	}

	public void notificarEliminar(String s) {
	}

	public void actionPerformed(ActionEvent e) {
	}
}
