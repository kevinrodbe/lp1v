package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;
import Controller.*;
import Model.*;

public class P_1_4_MenuReporte extends JPanel implements ActionListener {

	public JButton btnOk = new JButton("Ok");
	private DemoGUI demoGUI;
	private JTextField txtCod;
	private JLabel lblSalir_1_4;
	private JTextField txtFechaFin;
	private JTextField txtFechaInicio;
	private JTextArea txtS;
	private JComboBox cbo;
	public ArregloMascota aMascota = new ArregloMascota();
	public ArregloCliente aCliente = new ArregloCliente();
	public ArregloCajero aCajero = new ArregloCajero();
	public ArregloCita aCita = new ArregloCita();
	public ArregloAtencion aAtencion = new ArregloAtencion();
	public ArregloReceta aReceta = new ArregloReceta();

	public P_1_4_MenuReporte(final DemoGUI demoGUI) {
		setFont(new Font("Tahoma", Font.BOLD, 14));
		this.demoGUI = demoGUI;

		setSize(403, 380);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Reportes", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				procesar();
			}
		});

		btnOk.addActionListener(this);
		btnOk.setBounds(337, 21, 56, 50);
		add(btnOk);

		JLabel lblNewLabel = new JLabel("Reportes");
		lblNewLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(131, 21, 144, 25);
		add(lblNewLabel);

		lblSalir_1_4 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1);
			}
		});
		lblSalir_1_4.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_4.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(0,
				0, 0)));
		lblSalir_1_4.setBounds(10, 336, 70, 33);
		add(lblSalir_1_4);

		cbo = new JComboBox();
		cbo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (cbo.getSelectedIndex() == 2) {
					txtFechaInicio.setEditable(true);
					txtFechaFin.setEditable(true);
					txtCod.setEditable(false);
				} else {
					txtFechaInicio.setEditable(false);
					txtFechaFin.setEditable(false);
					txtCod.setEditable(true);
				}
			}
		});
		cbo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		cbo.setModel(new DefaultComboBoxModel(new String[] {
				"Atenciones por Cajero", "Atencion por Mascota",
				"Citas por rango de fechas", "Recetas por Atenci\u00F3n" }));
		cbo.setBounds(10, 57, 317, 20);
		add(cbo);

		JLabel lblCdigo = new JLabel("C\u00F3digo");
		lblCdigo.setBounds(10, 88, 46, 14);
		add(lblCdigo);

		txtCod = new JTextField();
		txtCod.setBounds(66, 85, 196, 20);
		add(txtCod);
		txtCod.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Fecha inicio");
		lblNewLabel_1.setBounds(10, 121, 70, 14);
		add(lblNewLabel_1);

		JLabel lblFechaFin = new JLabel("Fecha fin");
		lblFechaFin.setBounds(205, 121, 70, 14);
		add(lblFechaFin);

		txtFechaFin = new JTextField();
		txtFechaFin.setEditable(false);
		txtFechaFin.setBounds(268, 115, 101, 20);
		add(txtFechaFin);
		txtFechaFin.setColumns(10);

		txtFechaInicio = new JTextField();
		txtFechaInicio.setEditable(false);
		txtFechaInicio.setColumns(10);
		txtFechaInicio.setBounds(90, 115, 101, 20);
		add(txtFechaInicio);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 149, 383, 176);
		add(scrollPane);

		txtS = new JTextArea();
		txtS.setEditable(false);
		scrollPane.setViewportView(txtS);
		/*
		 * if (cbo.getSelectedIndex() == 2) { txtFechaInicio.setEditable(true);
		 * txtFechaFin.setEditable(true); } else {
		 * txtFechaInicio.setEditable(false); txtFechaFin.setEditable(false); }
		 */

		setVisible(false);
	}

	public void procesar() {
		switch (cbo.getSelectedIndex()) {
		case 0:
			if (txtCod.getText().trim().equals("")) {
				break;
			} else {
				atencionPorCajero();
				break;
			}
		case 1:
			if (txtCod.getText().trim().equals("")) {
				break;
			} else {
				atencionPorMascota();
				break;
			}
		case 2:
			if (txtFechaInicio.getText().trim().equals("")
					|| txtFechaFin.getText().trim().equals("")) {
				break;
			} else {
				citasPorFecha();
				break;
			}
		case 3:
			if (txtCod.getText().trim().equals("")) {
				break;
			} else {
				recetasPorAtencion();
				break;
			}
		}
	}

	public void limpiar() {
		txtCod.setText("");
		txtFechaInicio.setText("");
		txtFechaFin.setText("");
		txtS.setText("");
	}

	public void atencionPorCajero() {
		int codi = Integer.parseInt(txtCod.getText());
		String linea = "", estado = "", tipo = "";
		txtS.setText("");
		txtS.append("Cod. Atencion\tCod. Cita\tCod. Cliente\tCod. Mascota\tCod. Cajero\tTipo\tDiagnostico\tFec. Registro\tEstado\n");
		txtS.append("===================================================================================================================\n");
		for (int i = 0; i < aAtencion.tama�o(); i++) {
			if (codi == aAtencion.obtener(i).getCodCajero()) {
				switch (aAtencion.obtener(i).getEstado()) {
				case 0:
					estado = "Anulada - 0";
					break;
				case 1:
					estado = "Atendida - 1";
					break;
				}
				switch (aAtencion.obtener(i).getTipo()) {
				case 0:
					tipo = "Con cita - 0";
					break;
				case 1:
					tipo = "Sin cita - 1";
					break;
				}
				linea = aAtencion.obtener(i).getCodAtencion() + "\t"
						+ aAtencion.obtener(i).getCodCita() + "\t "
						+ aAtencion.obtener(i).getCodCliente() + "\t"
						+ aAtencion.obtener(i).getCodMascota() + " \t "
						+ aAtencion.obtener(i).getCodCajero() + "\t " + tipo
						+ "\t " + aAtencion.obtener(i).getDiagnostico() + "\t "
						+ aAtencion.obtener(i).getFechaRegistro() + " \t "
						+ estado + "\n";
				txtS.append(linea);
			} else {
				limpiar();
			}
		}
	}

	public void atencionPorMascota() {
		int codi = Integer.parseInt(txtCod.getText());
		String linea = "", estado = "", tipo = "";
		txtS.setText("");
		txtS.append("Cod. Atencion\tCod. Cita\tCod. Cliente\tCod. Mascota\tCod. Cajero\tTipo\tDiagnostico\tFec. Registro\tEstado\n");
		txtS.append("===================================================================================================================\n");
		for (int i = 0; i < aAtencion.tama�o(); i++) {
			if (codi == aAtencion.obtener(i).getCodMascota()) {
				switch (aAtencion.obtener(i).getEstado()) {
				case 0:
					estado = "Anulada - 0";
					break;
				case 1:
					estado = "Atendida - 1";
					break;
				}
				switch (aAtencion.obtener(i).getTipo()) {
				case 0:
					tipo = "Con cita - 0";
					break;
				case 1:
					tipo = "Sin cita - 1";
					break;
				}
				linea = aAtencion.obtener(i).getCodAtencion() + "\t"
						+ aAtencion.obtener(i).getCodCita() + "\t "
						+ aAtencion.obtener(i).getCodCliente() + "\t"
						+ aAtencion.obtener(i).getCodMascota() + " \t "
						+ aAtencion.obtener(i).getCodCajero() + "\t " + tipo
						+ "\t " + aAtencion.obtener(i).getDiagnostico() + "\t "
						+ aAtencion.obtener(i).getFechaRegistro() + " \t "
						+ estado + "\n";
				txtS.append(linea);
			} else {
				limpiar();
			}
		}
	}

	public void citasPorFecha() {

	}

	public void recetasPorAtencion() {
		int codi = Integer.parseInt(txtCod.getText());
		String linea = "";
		double subtotal;
		txtS.setText("");
		txtS.append("Cod. Atencion\tCod. Medicamento\tCantidad\tPrecio\tSub total\n");
		txtS.append("=======================================================================\n");
		for (int i = 0; i < aReceta.tama�o(); i++) {
			if (codi == aReceta.obtener(i).getCodAtencion()) {
				subtotal = aReceta.obtener(i).getCantidad()
						* aReceta.obtener(i).getPrecio();
				linea = aReceta.obtener(i).getCodMedicamento() + "\t"
						+ aReceta.obtener(i).getCantidad() + "\t "
						+ aReceta.obtener(i).getPrecio() + "\t" + subtotal
						+ "\n";
				txtS.append(linea);
			} else {
				limpiar();
			}
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == lblSalir_1_4) {
			demoGUI.habilita(DemoGUI.PANEL_1);
		}
	}
}
