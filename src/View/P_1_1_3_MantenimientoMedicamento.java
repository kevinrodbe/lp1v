package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;
import Controller.Medicamento;
import Model.ArregloMedicamento;

public class P_1_1_3_MantenimientoMedicamento extends JPanel implements
		ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtName_pIngresar;
	private JTextField txtLab_pIngresar;
	private JTextField txtPrecio_pIngresar;
	private JTextField txtStock_pIngresar;
	private JTextField txtCod_pIngresar;
	private JTextField txtCod_pEliminar;
	private JTextField txtCod_pModificar;
	private JTextField txtCod_pConsultar;
	private JTextField txtPrecio_pModificar;
	private JTextField txtName_pModificar;
	private JTextField txtStock_pModificar;
	private JTextField txtLab_pModificar;
	private JTextField txtPrecio_pEliminar;
	private JTextField txtName_pEliminar;
	private JTextField txtStock_pEliminar;
	private JTextField txtLab_pEliminar;
	private JTextField txtPrecio_pConsultar;
	private JTextField txtName_pConsultar;
	private JTextField txtStock_pConsultar;
	private JTextField txtLab_pConsultar;
	private JLabel lblNotificar_pIngresar;
	private JLabel lblNotificar_pModificar;
	private JLabel lblNotificar_pEliminar;
	private JLabel lblNotificar_pConsultar;
	private JTextArea txtS;

	public ArregloMedicamento aMedicamento = new ArregloMedicamento();

	public P_1_1_3_MantenimientoMedicamento(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(589, 344);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Mantenimiento - Medicamento", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel lblMantenimientoCajeros = new JLabel(
				"Mantenimiento - Medicamento");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		JLabel lblSalir_1_1_3 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_1_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		lblSalir_1_1_3.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_1_3.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_1_3.setBounds(10, 300, 70, 33);
		add(lblSalir_1_1_3);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 546, 231);
		add(tabbedPane);

		JPanel pnl_Ingresar_Manto = new JPanel();
		pnl_Ingresar_Manto.setToolTipText("");
		tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Manto, null);
		pnl_Ingresar_Manto.setLayout(null);

		JLabel lblIngresarCajeros = new JLabel("Ingresar Medicamento");
		lblIngresarCajeros.setBounds(10, 11, 219, 22);
		lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarCajeros.setForeground(Color.BLUE);
		lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblIngresarCajeros.setAlignmentX(0.5f);
		pnl_Ingresar_Manto.add(lblIngresarCajeros);

		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(10, 57, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Laboratorio");
		lblNewLabel_1.setBounds(229, 57, 106, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Precio");
		lblNewLabel_2.setBounds(10, 87, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Stock");
		lblNewLabel_4.setBounds(229, 87, 106, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_4);

		txtName_pIngresar = new JTextField();
		txtName_pIngresar.setBounds(66, 53, 153, 20);
		pnl_Ingresar_Manto.add(txtName_pIngresar);
		txtName_pIngresar.setColumns(10);

		txtLab_pIngresar = new JTextField();
		txtLab_pIngresar.setBounds(345, 50, 185, 20);
		pnl_Ingresar_Manto.add(txtLab_pIngresar);
		txtLab_pIngresar.setColumns(10);

		txtPrecio_pIngresar = new JTextField();
		txtPrecio_pIngresar.setBounds(66, 83, 153, 20);
		pnl_Ingresar_Manto.add(txtPrecio_pIngresar);
		txtPrecio_pIngresar.setColumns(10);

		txtStock_pIngresar = new JTextField();
		txtStock_pIngresar.setColumns(10);
		txtStock_pIngresar.setBounds(345, 81, 185, 20);
		pnl_Ingresar_Manto.add(txtStock_pIngresar);

		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				registrarMedicamento();
			}
		});
		btnRegistrar.setBounds(365, 152, 165, 40);
		pnl_Ingresar_Manto.add(btnRegistrar);

		JLabel lblCdigo = new JLabel("C\u00F3digo");
		lblCdigo.setBounds(239, 18, 106, 14);
		pnl_Ingresar_Manto.add(lblCdigo);

		txtCod_pIngresar = new JTextField();
		txtCod_pIngresar.setEditable(false);
		txtCod_pIngresar.setBounds(345, 15, 185, 20);
		pnl_Ingresar_Manto.add(txtCod_pIngresar);
		txtCod_pIngresar.setColumns(10);

		lblNotificar_pIngresar = new JLabel("");
		lblNotificar_pIngresar.setForeground(Color.RED);
		lblNotificar_pIngresar.setBounds(10, 178, 325, 14);
		pnl_Ingresar_Manto.add(lblNotificar_pIngresar);

		JPanel pnl_Modificar_Manto = new JPanel();
		tabbedPane.addTab("Modificar", null, pnl_Modificar_Manto, null);
		pnl_Modificar_Manto.setLayout(null);

		JLabel lblModificarCajeros = new JLabel("Modificar Medicamento");
		lblModificarCajeros.setBounds(10, 11, 218, 22);
		lblModificarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblModificarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblModificarCajeros.setForeground(Color.BLUE);
		lblModificarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblModificarCajeros.setAlignmentX(0.5f);
		pnl_Modificar_Manto.add(lblModificarCajeros);

		JButton btnBuscar_pModificar = new JButton("b");
		btnBuscar_pModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				buscarMedicamento_pModificar();
			}
		});
		btnBuscar_pModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBuscar_pModificar.setBounds(491, 11, 39, 26);
		pnl_Modificar_Manto.add(btnBuscar_pModificar);

		txtCod_pModificar = new JTextField();
		txtCod_pModificar.setColumns(10);
		txtCod_pModificar.setBounds(353, 15, 128, 20);
		pnl_Modificar_Manto.add(txtCod_pModificar);

		JLabel lblCdigo_1 = new JLabel("C\u00F3digo Medicamento");
		lblCdigo_1.setBounds(229, 18, 128, 14);
		pnl_Modificar_Manto.add(lblCdigo_1);

		JButton btnGrabar_pModificar = new JButton("Grabar");
		btnGrabar_pModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				actualizarMedicamento();
			}
		});
		btnGrabar_pModificar.setBounds(365, 152, 165, 40);
		pnl_Modificar_Manto.add(btnGrabar_pModificar);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 44, 541, 9);
		pnl_Modificar_Manto.add(separator);

		JLabel label_1 = new JLabel("Nombre");
		label_1.setBounds(10, 69, 46, 14);
		pnl_Modificar_Manto.add(label_1);

		JLabel label_2 = new JLabel("Precio");
		label_2.setBounds(10, 99, 46, 14);
		pnl_Modificar_Manto.add(label_2);

		txtPrecio_pModificar = new JTextField();
		txtPrecio_pModificar.setColumns(10);
		txtPrecio_pModificar.setBounds(66, 95, 153, 20);
		pnl_Modificar_Manto.add(txtPrecio_pModificar);

		txtName_pModificar = new JTextField();
		txtName_pModificar.setColumns(10);
		txtName_pModificar.setBounds(66, 65, 153, 20);
		pnl_Modificar_Manto.add(txtName_pModificar);

		JLabel label_3 = new JLabel("Laboratorio");
		label_3.setBounds(229, 69, 106, 14);
		pnl_Modificar_Manto.add(label_3);

		JLabel label_4 = new JLabel("Stock");
		label_4.setBounds(229, 99, 106, 14);
		pnl_Modificar_Manto.add(label_4);

		txtStock_pModificar = new JTextField();
		txtStock_pModificar.setColumns(10);
		txtStock_pModificar.setBounds(345, 93, 185, 20);
		pnl_Modificar_Manto.add(txtStock_pModificar);

		txtLab_pModificar = new JTextField();
		txtLab_pModificar.setColumns(10);
		txtLab_pModificar.setBounds(345, 62, 185, 20);
		pnl_Modificar_Manto.add(txtLab_pModificar);

		lblNotificar_pModificar = new JLabel("");
		lblNotificar_pModificar.setForeground(Color.RED);
		lblNotificar_pModificar.setBounds(10, 178, 325, 14);
		pnl_Modificar_Manto.add(lblNotificar_pModificar);

		JPanel pnl_Eliminar_Manto = new JPanel();
		tabbedPane.addTab("Eliminar", null, pnl_Eliminar_Manto, null);
		pnl_Eliminar_Manto.setLayout(null);

		JLabel lblEliminarCajeros = new JLabel("Eliminar Medicamento");
		lblEliminarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEliminarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblEliminarCajeros.setForeground(Color.BLUE);
		lblEliminarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblEliminarCajeros.setAlignmentX(0.5f);
		lblEliminarCajeros.setBounds(10, 11, 209, 22);
		pnl_Eliminar_Manto.add(lblEliminarCajeros);

		txtCod_pEliminar = new JTextField();
		txtCod_pEliminar.setColumns(10);
		txtCod_pEliminar.setBounds(365, 15, 116, 20);
		pnl_Eliminar_Manto.add(txtCod_pEliminar);

		JButton btnBuscar_pEliminar = new JButton("b");
		btnBuscar_pEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarMedicamento_pEliminar();
			}
		});
		btnBuscar_pEliminar.setBounds(491, 11, 39, 26);
		pnl_Eliminar_Manto.add(btnBuscar_pEliminar);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 43, 541, 9);
		pnl_Eliminar_Manto.add(separator_1);

		JLabel lblCdigoMedicamento = new JLabel("C\u00F3digo medicamento");
		lblCdigoMedicamento.setBounds(218, 18, 153, 14);
		pnl_Eliminar_Manto.add(lblCdigoMedicamento);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				eliminarMedicamento();
			}
		});
		btnEliminar.setBounds(365, 152, 165, 40);
		pnl_Eliminar_Manto.add(btnEliminar);

		JLabel label_5 = new JLabel("Nombre");
		label_5.setBounds(10, 70, 46, 14);
		pnl_Eliminar_Manto.add(label_5);

		JLabel label_6 = new JLabel("Precio");
		label_6.setBounds(10, 100, 46, 14);
		pnl_Eliminar_Manto.add(label_6);

		txtPrecio_pEliminar = new JTextField();
		txtPrecio_pEliminar.setEditable(false);
		txtPrecio_pEliminar.setColumns(10);
		txtPrecio_pEliminar.setBounds(66, 96, 153, 20);
		pnl_Eliminar_Manto.add(txtPrecio_pEliminar);

		txtName_pEliminar = new JTextField();
		txtName_pEliminar.setEditable(false);
		txtName_pEliminar.setColumns(10);
		txtName_pEliminar.setBounds(66, 66, 153, 20);
		pnl_Eliminar_Manto.add(txtName_pEliminar);

		JLabel label_7 = new JLabel("Laboratorio");
		label_7.setBounds(229, 70, 106, 14);
		pnl_Eliminar_Manto.add(label_7);

		JLabel label_8 = new JLabel("Stock");
		label_8.setBounds(229, 100, 106, 14);
		pnl_Eliminar_Manto.add(label_8);

		txtStock_pEliminar = new JTextField();
		txtStock_pEliminar.setEditable(false);
		txtStock_pEliminar.setColumns(10);
		txtStock_pEliminar.setBounds(345, 94, 185, 20);
		pnl_Eliminar_Manto.add(txtStock_pEliminar);

		txtLab_pEliminar = new JTextField();
		txtLab_pEliminar.setEditable(false);
		txtLab_pEliminar.setColumns(10);
		txtLab_pEliminar.setBounds(345, 63, 185, 20);
		pnl_Eliminar_Manto.add(txtLab_pEliminar);

		lblNotificar_pEliminar = new JLabel("");
		lblNotificar_pEliminar.setForeground(Color.RED);
		lblNotificar_pEliminar.setBounds(10, 178, 325, 14);
		pnl_Eliminar_Manto.add(lblNotificar_pEliminar);

		JPanel pnl_Consultar_Manto = new JPanel();
		tabbedPane.addTab("Consultar", null, pnl_Consultar_Manto, null);
		pnl_Consultar_Manto.setLayout(null);

		JLabel lblConsultarCajeros = new JLabel("Consultar Medicamento");
		lblConsultarCajeros.setBounds(10, 11, 216, 22);
		lblConsultarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblConsultarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsultarCajeros.setForeground(Color.BLUE);
		lblConsultarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblConsultarCajeros.setAlignmentX(0.5f);
		pnl_Consultar_Manto.add(lblConsultarCajeros);

		JButton btnBuscar_pConsultar = new JButton("b");
		btnBuscar_pConsultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				consultarMedicamento();
			}
		});
		btnBuscar_pConsultar.setBounds(491, 11, 39, 26);
		pnl_Consultar_Manto.add(btnBuscar_pConsultar);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 42, 541, 9);
		pnl_Consultar_Manto.add(separator_2);

		JLabel lblCdigoMedicamento_1 = new JLabel("C\u00F3digo medicamento");
		lblCdigoMedicamento_1.setBounds(229, 18, 133, 14);
		pnl_Consultar_Manto.add(lblCdigoMedicamento_1);

		txtCod_pConsultar = new JTextField();
		txtCod_pConsultar.setColumns(10);
		txtCod_pConsultar.setBounds(365, 15, 116, 20);
		pnl_Consultar_Manto.add(txtCod_pConsultar);

		JLabel label = new JLabel("Nombre");
		label.setBounds(10, 85, 46, 14);
		pnl_Consultar_Manto.add(label);

		JLabel label_9 = new JLabel("Precio");
		label_9.setBounds(10, 115, 46, 14);
		pnl_Consultar_Manto.add(label_9);

		txtPrecio_pConsultar = new JTextField();
		txtPrecio_pConsultar.setEditable(false);
		txtPrecio_pConsultar.setColumns(10);
		txtPrecio_pConsultar.setBounds(66, 111, 153, 20);
		pnl_Consultar_Manto.add(txtPrecio_pConsultar);

		txtName_pConsultar = new JTextField();
		txtName_pConsultar.setEditable(false);
		txtName_pConsultar.setColumns(10);
		txtName_pConsultar.setBounds(66, 81, 153, 20);
		pnl_Consultar_Manto.add(txtName_pConsultar);

		JLabel label_10 = new JLabel("Laboratorio");
		label_10.setBounds(229, 85, 106, 14);
		pnl_Consultar_Manto.add(label_10);

		JLabel label_11 = new JLabel("Stock");
		label_11.setBounds(229, 115, 106, 14);
		pnl_Consultar_Manto.add(label_11);

		txtStock_pConsultar = new JTextField();
		txtStock_pConsultar.setEditable(false);
		txtStock_pConsultar.setColumns(10);
		txtStock_pConsultar.setBounds(345, 109, 185, 20);
		pnl_Consultar_Manto.add(txtStock_pConsultar);

		txtLab_pConsultar = new JTextField();
		txtLab_pConsultar.setEditable(false);
		txtLab_pConsultar.setColumns(10);
		txtLab_pConsultar.setBounds(345, 78, 185, 20);
		pnl_Consultar_Manto.add(txtLab_pConsultar);

		lblNotificar_pConsultar = new JLabel("");
		lblNotificar_pConsultar.setForeground(Color.RED);
		lblNotificar_pConsultar.setBounds(10, 178, 325, 14);
		pnl_Consultar_Manto.add(lblNotificar_pConsultar);

		JPanel pnl_Listar_Manto = new JPanel();
		tabbedPane.addTab("Listar", null, pnl_Listar_Manto, null);
		pnl_Listar_Manto.setLayout(null);

		JLabel lblListarCajeros = new JLabel("Listar Medicamento");
		lblListarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblListarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarCajeros.setForeground(Color.BLUE);
		lblListarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarCajeros.setAlignmentX(0.5f);
		lblListarCajeros.setBounds(10, 11, 201, 22);
		pnl_Listar_Manto.add(lblListarCajeros);

		JButton btnNewButton_1 = new JButton("Listar");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				listarMedicamento();
			}
		});
		btnNewButton_1.setBounds(173, 40, 89, 23);
		pnl_Listar_Manto.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 74, 521, 118);
		pnl_Listar_Manto.add(scrollPane);

		txtS = new JTextArea();
		txtS.setEditable(false);
		scrollPane.setViewportView(txtS);

		txtCod_pIngresar.setText(generarID() + "");

		setVisible(false);
	}

	public void registrarMedicamento() {
		String name, lab;
		int cod, stock;
		double preUni;
		cod = Integer.parseInt(txtCod_pIngresar.getText());
		name = txtName_pIngresar.getText();
		lab = txtLab_pIngresar.getText();
		preUni = Double.parseDouble(txtPrecio_pIngresar.getText());
		stock = Integer.parseInt(txtStock_pIngresar.getText());
		Medicamento objM = new Medicamento(cod, name, lab, preUni, stock);
		aMedicamento.agregar(objM);
		aMedicamento.grabarMedicamento();
		limpiarPNL_ingresar();
		notificarIngresar("Medicamento " + cod + " registrado");
	}

	public void buscarMedicamento_pModificar() {
		int codi = Integer.parseInt(txtCod_pModificar.getText());
		Medicamento objM = aMedicamento.buscar(codi);
		if (objM == null) {
			notificarModificar("Medicamento " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			txtName_pModificar.setText(objM.getNameMedicamento());
			txtPrecio_pModificar.setText(objM.getPreUni() + "");
			txtLab_pModificar.setText(objM.getLaboratorio());
			txtStock_pModificar.setText(objM.getStock() + "");
			notificarModificar("Medicamento " + codi + " encontrado");
		}
	}

	public void actualizarMedicamento() {
		int codi = Integer.parseInt(txtCod_pModificar.getText());
		Medicamento objM = aMedicamento.buscar(codi);
		if (objM == null) {
			notificarModificar("Medicamento " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			objM.setNameMedicamento(txtName_pModificar.getText());
			objM.setPreUni(Double.parseDouble(txtPrecio_pModificar.getText()));
			objM.setLaboratorio(txtLab_pModificar.getText());
			objM.setStock(Integer.parseInt(txtStock_pModificar.getText()));
			aMedicamento.grabarMedicamento();
			notificarModificar("Medicamento " + codi + " actualizado");
		}
	}

	public void buscarMedicamento_pEliminar() {
		int codi = Integer.parseInt(txtCod_pEliminar.getText());
		Medicamento objM = aMedicamento.buscar(codi);
		if (objM == null) {
			notificarEliminar("Medicamento " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			txtName_pEliminar.setText(objM.getNameMedicamento());
			txtLab_pEliminar.setText(objM.getLaboratorio());
			txtPrecio_pEliminar.setText(objM.getPreUni() + "");
			txtStock_pEliminar.setText(objM.getStock() + "");
			notificarEliminar("Medicamento " + codi + " encontrado");
		}
	}

	public void eliminarMedicamento() {
		int codi = Integer.parseInt(txtCod_pEliminar.getText());
		Medicamento objM = aMedicamento.buscar(codi);
		if (objM == null) {
			notificarEliminar("Medicamento " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			aMedicamento.eliminar(objM);
			aMedicamento.grabarMedicamento();
			notificarEliminar("Medicamento " + codi + " eliminado");
		}
	}

	public void consultarMedicamento() {
		int codi = Integer.parseInt(txtCod_pConsultar.getText());
		Medicamento objM = aMedicamento.buscar(codi);
		if (objM == null) {
			notificarConsultar("Medicamento " + codi + " NO existe");
			limpiarPNL_consultar();
		} else {
			txtName_pConsultar.setText(objM.getNameMedicamento());
			txtLab_pConsultar.setText(objM.getLaboratorio());
			txtPrecio_pConsultar.setText(objM.getPreUni() + "");
			txtStock_pConsultar.setText(objM.getStock() + "");
			notificarConsultar("Medicamento " + codi + " encontrado");
		}
	}

	public void listarMedicamento() {
		String linea = "", estado = "";
		txtS.setText("");
		txtS.append("C�digo \tNombre \tLaboratorio \tPrecio \tStock\n");
		txtS.append("===========================================================\n");
		for (int i = 0; i < aMedicamento.tama�o(); i++) {
			linea = aMedicamento.obtener(i).getCodMedicamento() + "\t"
					+ aMedicamento.obtener(i).getNameMedicamento() + "\t"
					+ aMedicamento.obtener(i).getLaboratorio() + "\t"
					+ aMedicamento.obtener(i).getPreUni() + "\t"
					+ aMedicamento.obtener(i).getStock() + "\t" + "\n";
			txtS.append(linea);
		}
	}

	public int generarID() {
		int canti = aMedicamento.tama�o();
		int id;
		if (canti == 0) {
			id = 1;
		} else {
			id = canti + 1;
		}
		System.out.println(canti + "------id: " + id);
		return id;
	}

	public void limpiarPNL_ingresar() {
		txtCod_pIngresar.setText(generarID() + "");
		txtName_pIngresar.setText("");
		txtPrecio_pIngresar.setText("");
		txtLab_pIngresar.setText("");
		txtStock_pIngresar.setText("");
	}

	public void limpiarPNL_modificar() {
		txtCod_pModificar.setText("");
		txtName_pModificar.setText("");
		txtPrecio_pModificar.setText("");
		txtLab_pModificar.setText("");
		txtStock_pModificar.setText("");
	}

	public void limpiarPNL_eliminar() {
		txtCod_pEliminar.setText("");
		txtName_pEliminar.setText("");
		txtPrecio_pEliminar.setText("");
		txtLab_pEliminar.setText("");
		txtStock_pEliminar.setText("");
	}

	public void limpiarPNL_consultar() {
		txtCod_pConsultar.setText("");
		txtName_pConsultar.setText("");
		txtPrecio_pConsultar.setText("");
		txtLab_pConsultar.setText("");
		txtStock_pConsultar.setText("");
	}

	public void notificarIngresar(String s) {
		lblNotificar_pIngresar.setText(s);
	}

	public void notificarModificar(String s) {
		lblNotificar_pModificar.setText(s);
	}

	public void notificarEliminar(String s) {
		lblNotificar_pEliminar.setText(s);
	}

	public void notificarConsultar(String s) {
		lblNotificar_pConsultar.setText(s);
	}

	public void actionPerformed(ActionEvent e) {
	}
}
