package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class P_1_Principal_logistica extends JPanel implements ActionListener {

	public JButton btnCuadroNecesidad = new JButton("Realizar cuadro de necesidad");
	public JButton btnEmpresas = new JButton("Empresas");
	private DemoGUI demoGUI;
	public final JButton btnVerPedidos = new JButton("Ver pedidos");
	public final JButton btnSalir = new JButton("Salir");

	public P_1_Principal_logistica(final DemoGUI demoGUI) {
		setSize(new Dimension(400, 380));// nadaa
		setBounds(new Rectangle(0, 0, 400, 380));// nadaa
		this.demoGUI = demoGUI;

		// demoGUI.setSize(600, 900);
		setSize(300, 380);// tama�o del panel
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Men\u00FA Principal", TitledBorder.LEADING, TitledBorder.TOP,
				null, Color.BLACK));

		btnCuadroNecesidad.addActionListener(this);
		btnCuadroNecesidad.setBounds(24, 126, 250, 50);
		add(btnCuadroNecesidad);

		btnEmpresas.addActionListener(this);
		btnEmpresas.setBounds(24, 186, 250, 50);
		add(btnEmpresas);

		JLabel lblMenPrincipal = new JLabel("Men\u00FA Principal");
		lblMenPrincipal.setHorizontalAlignment(SwingConstants.CENTER);
		lblMenPrincipal.setForeground(Color.BLUE);
		lblMenPrincipal.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMenPrincipal.setBounds(82, 24, 141, 31);
		add(lblMenPrincipal);
		btnVerPedidos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		btnVerPedidos.setBounds(24, 66, 250, 50);

		add(btnVerPedidos);
		btnSalir.setBounds(24, 319, 250, 50);

		add(btnSalir);

		setVisible(false);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnVerPedidos) {
			demoGUI.habilita(DemoGUI.PANEL_1_1);
		}
		if (e.getSource() == btnCuadroNecesidad) {
			demoGUI.habilita(DemoGUI.PANEL_1_2);
		}
		if (e.getSource() == btnEmpresas) {
			demoGUI.habilita(DemoGUI.PANEL_1_3);
		}
	}
}
