package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;
import Controller.Mascota;
import Model.ArregloMascota;
import Controller.Cliente;
import Model.ArregloCliente;

public class P_1_1_2_MantenimientoMascota extends JPanel implements
		ActionListener {
	private DemoGUI demoGUI;
	private JTextField txtNameMascota_pIngresar;
	private JTextField txtRazaMascota_pIngresar;
	private JTextField txtTipoMascota_pIngresar;
	private JTextField txtFechaNacMascota_pIngresar;
	private JTextField txtCodMascota_pIngresar;
	private JTextField txtCodMascota_pEliminar;
	private JTextField txtRaza_pModificar;
	private JTextField txtName_pModificar;
	private JTextField txtFecha_pModificar;
	private JTextField txtTipo_pModificar;
	private JTextField txtCodMascota_pModificar;
	private JTextField txtRaza_pEliminar;
	private JTextField txtName_pEliminar;
	private JTextField txtFecha_pEliminar;
	private JTextField txtTipo_pEliminar;
	private JTextField txtCodCliente_pEliminar;
	private JTextField txtRaza_pConsultar;
	private JTextField txtName_pConsultar;
	private JTextField txtFecha_pConsultar;
	private JTextField txtTipo_pConsultar;
	private JTextField txtCodCliente_pConsultar;
	private JTextField txtCodMascota_pConsultar;
	private JComboBox cboEstadoMascota_pIngresar;

	public ArregloMascota aMascota = new ArregloMascota();
	public ArregloCliente aCliente = new ArregloCliente();
	private JComboBox cboCliente_pIngresar;
	private JLabel lblNotificar_ingresar;
	private JLabel lblNotificar_modificar;
	private JLabel lblNotificar_eliminar;
	private JLabel lblNotificar_consultar;
	private JComboBox cboEstado_pModificar;
	private JComboBox cboCliente_pModificar;
	private JComboBox cboEstado_pEliminar;
	private JComboBox cboEstado_pConsultar;
	private JTextArea txtS;

	public P_1_1_2_MantenimientoMascota(final DemoGUI demoGUI) {
		this.demoGUI = demoGUI;

		setSize(589, 344);
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Mantenimiento - Mascotas", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel lblMantenimientoCajeros = new JLabel("Mantenimiento - Mascotas");
		lblMantenimientoCajeros
				.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMantenimientoCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblMantenimientoCajeros.setForeground(Color.ORANGE);
		lblMantenimientoCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMantenimientoCajeros.setAlignmentX(0.5f);
		lblMantenimientoCajeros.setBounds(47, 26, 280, 25);
		add(lblMantenimientoCajeros);

		JLabel lblSalir_1_1_2 = new JLabel("<< Atr\u00E1s");
		lblSalir_1_1_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		lblSalir_1_1_2.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblSalir_1_1_2.setBorder(new MatteBorder(0, 0, 0, 3, (Color) new Color(

		0, 0, 0)));
		lblSalir_1_1_2.setBounds(10, 300, 70, 33);
		add(lblSalir_1_1_2);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(10, 58, 569, 231);
		add(tabbedPane);

		JPanel pnl_Ingresar_Manto = new JPanel();
		pnl_Ingresar_Manto.setToolTipText("");
		tabbedPane.addTab("Ingresar", null, pnl_Ingresar_Manto, null);
		pnl_Ingresar_Manto.setLayout(null);

		JLabel lblIngresarCajeros = new JLabel("Ingresar Mascotas");
		lblIngresarCajeros.setBounds(10, 11, 171, 22);
		lblIngresarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblIngresarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarCajeros.setForeground(Color.BLUE);
		lblIngresarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblIngresarCajeros.setAlignmentX(0.5f);
		pnl_Ingresar_Manto.add(lblIngresarCajeros);

		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(20, 44, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("C\u00F3digo de Cliente");
		lblNewLabel_1.setBounds(239, 44, 106, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Raza");
		lblNewLabel_2.setBounds(20, 74, 46, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Fecha de nacimiento");
		lblNewLabel_3.setBounds(239, 104, 128, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_3);

		JLabel lblPassword = new JLabel("Estado");
		lblPassword.setBounds(20, 104, 46, 14);
		pnl_Ingresar_Manto.add(lblPassword);

		JLabel lblNewLabel_4 = new JLabel("Tipo de animal");
		lblNewLabel_4.setBounds(239, 74, 106, 14);
		pnl_Ingresar_Manto.add(lblNewLabel_4);

		txtNameMascota_pIngresar = new JTextField();
		txtNameMascota_pIngresar.setBounds(76, 40, 153, 20);
		pnl_Ingresar_Manto.add(txtNameMascota_pIngresar);
		txtNameMascota_pIngresar.setColumns(10);

		txtRazaMascota_pIngresar = new JTextField();
		txtRazaMascota_pIngresar.setBounds(76, 70, 153, 20);
		pnl_Ingresar_Manto.add(txtRazaMascota_pIngresar);
		txtRazaMascota_pIngresar.setColumns(10);

		txtTipoMascota_pIngresar = new JTextField();
		txtTipoMascota_pIngresar.setColumns(10);
		txtTipoMascota_pIngresar.setBounds(365, 69, 185, 20);
		pnl_Ingresar_Manto.add(txtTipoMascota_pIngresar);

		txtFechaNacMascota_pIngresar = new JTextField();
		txtFechaNacMascota_pIngresar.setColumns(10);
		txtFechaNacMascota_pIngresar.setBounds(365, 98, 185, 20);
		pnl_Ingresar_Manto.add(txtFechaNacMascota_pIngresar);

		cboEstadoMascota_pIngresar = new JComboBox();
		cboEstadoMascota_pIngresar.setModel(new DefaultComboBoxModel(
				new String[] { "0 - Vivo", "1 - Enfermo", "2 - Fallecido" }));
		cboEstadoMascota_pIngresar.setBounds(76, 101, 153, 20);
		pnl_Ingresar_Manto.add(cboEstadoMascota_pIngresar);

		JButton btnRegistrarMascota = new JButton("Registrar");
		btnRegistrarMascota.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				registrarMascota();
			}
		});
		btnRegistrarMascota.setBounds(387, 152, 165, 40);
		pnl_Ingresar_Manto.add(btnRegistrarMascota);

		JLabel lblCdigo = new JLabel("C\u00F3digo Mascota");
		lblCdigo.setBounds(239, 11, 106, 14);
		pnl_Ingresar_Manto.add(lblCdigo);

		txtCodMascota_pIngresar = new JTextField();
		txtCodMascota_pIngresar.setEditable(false);
		txtCodMascota_pIngresar.setBounds(365, 8, 185, 20);
		pnl_Ingresar_Manto.add(txtCodMascota_pIngresar);
		txtCodMascota_pIngresar.setColumns(10);

		cboCliente_pIngresar = new JComboBox();
		cboCliente_pIngresar.setBounds(365, 42, 185, 20);
		pnl_Ingresar_Manto.add(cboCliente_pIngresar);

		for (int i = 0; i < aCliente.tama�o(); i++) {
			cboCliente_pIngresar.addItem(aCliente.obtener(i).getCodCliente());
		}

		lblNotificar_ingresar = new JLabel("");
		lblNotificar_ingresar.setForeground(Color.RED);
		lblNotificar_ingresar.setBounds(20, 178, 325, 14);
		pnl_Ingresar_Manto.add(lblNotificar_ingresar);

		JPanel pnl_Modificar_Manto = new JPanel();
		tabbedPane.addTab("Modificar", null, pnl_Modificar_Manto, null);
		pnl_Modificar_Manto.setLayout(null);

		JLabel lblModificarCajeros = new JLabel("Modificar Mascota");
		lblModificarCajeros.setBounds(10, 11, 175, 22);
		lblModificarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblModificarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblModificarCajeros.setForeground(Color.BLUE);
		lblModificarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblModificarCajeros.setAlignmentX(0.5f);
		pnl_Modificar_Manto.add(lblModificarCajeros);

		JButton btnBuscar_pModificar = new JButton("b");
		btnBuscar_pModificar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				buscarMascota_pModificar();
			}
		});
		btnBuscar_pModificar.setBounds(515, 12, 39, 26);
		pnl_Modificar_Manto.add(btnBuscar_pModificar);

		JLabel label_1 = new JLabel("Nombre");
		label_1.setBounds(10, 67, 46, 14);
		pnl_Modificar_Manto.add(label_1);

		JLabel label_2 = new JLabel("Raza");
		label_2.setBounds(10, 97, 46, 14);
		pnl_Modificar_Manto.add(label_2);

		JLabel label_3 = new JLabel("Estado");
		label_3.setBounds(10, 127, 46, 14);
		pnl_Modificar_Manto.add(label_3);

		cboEstado_pModificar = new JComboBox();
		cboEstado_pModificar.setModel(new DefaultComboBoxModel(new String[] {
				"0 - Vivo", "1 - Enfermo", "2 - Fallecido" }));
		cboEstado_pModificar.setBounds(66, 124, 153, 20);
		pnl_Modificar_Manto.add(cboEstado_pModificar);

		txtRaza_pModificar = new JTextField();
		txtRaza_pModificar.setColumns(10);
		txtRaza_pModificar.setBounds(66, 93, 153, 20);
		pnl_Modificar_Manto.add(txtRaza_pModificar);

		txtName_pModificar = new JTextField();
		txtName_pModificar.setColumns(10);
		txtName_pModificar.setBounds(66, 63, 153, 20);
		pnl_Modificar_Manto.add(txtName_pModificar);

		JLabel label_4 = new JLabel("C\u00F3digo de Cliente");
		label_4.setBounds(229, 67, 106, 14);
		pnl_Modificar_Manto.add(label_4);

		JLabel label_5 = new JLabel("Tipo de animal");
		label_5.setBounds(229, 97, 106, 14);
		pnl_Modificar_Manto.add(label_5);

		JLabel label_6 = new JLabel("Fecha de nacimiento");
		label_6.setBounds(229, 127, 126, 14);
		pnl_Modificar_Manto.add(label_6);

		txtFecha_pModificar = new JTextField();
		txtFecha_pModificar.setColumns(10);
		txtFecha_pModificar.setBounds(365, 120, 189, 20);
		pnl_Modificar_Manto.add(txtFecha_pModificar);

		txtTipo_pModificar = new JTextField();
		txtTipo_pModificar.setColumns(10);
		txtTipo_pModificar.setBounds(365, 91, 189, 20);
		pnl_Modificar_Manto.add(txtTipo_pModificar);

		txtCodMascota_pModificar = new JTextField();
		txtCodMascota_pModificar.setColumns(10);
		txtCodMascota_pModificar.setBounds(345, 11, 160, 20);
		pnl_Modificar_Manto.add(txtCodMascota_pModificar);

		JLabel label_7 = new JLabel("C\u00F3digo Mascota");
		label_7.setBounds(229, 16, 106, 14);
		pnl_Modificar_Manto.add(label_7);

		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				actualizarMascota();
			}
		});
		btnGrabar.setBounds(389, 152, 165, 40);
		pnl_Modificar_Manto.add(btnGrabar);

		JSeparator separator = new JSeparator();
		separator.setBounds(0, 44, 541, 9);
		pnl_Modificar_Manto.add(separator);

		lblNotificar_modificar = new JLabel("");
		lblNotificar_modificar.setForeground(Color.RED);
		lblNotificar_modificar.setBounds(10, 178, 325, 14);
		pnl_Modificar_Manto.add(lblNotificar_modificar);

		cboCliente_pModificar = new JComboBox();
		cboCliente_pModificar.setBounds(365, 64, 189, 20);
		pnl_Modificar_Manto.add(cboCliente_pModificar);
		for (int i = 0; i < aCliente.tama�o(); i++) {
			cboCliente_pModificar.addItem(aCliente.obtener(i).getCodCliente());
		}

		JPanel pnl_Eliminar_Manto = new JPanel();
		tabbedPane.addTab("Eliminar", null, pnl_Eliminar_Manto, null);
		pnl_Eliminar_Manto.setLayout(null);

		JLabel lblEliminarCajeros = new JLabel("Eliminar Cajeros");
		lblEliminarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEliminarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblEliminarCajeros.setForeground(Color.BLUE);
		lblEliminarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblEliminarCajeros.setAlignmentX(0.5f);
		lblEliminarCajeros.setBounds(10, 11, 171, 22);
		pnl_Eliminar_Manto.add(lblEliminarCajeros);

		txtCodMascota_pEliminar = new JTextField();
		txtCodMascota_pEliminar.setColumns(10);
		txtCodMascota_pEliminar.setBounds(345, 15, 160, 20);
		pnl_Eliminar_Manto.add(txtCodMascota_pEliminar);

		JButton button = new JButton("b");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarMascota_pEliminar();
			}
		});
		button.setBounds(515, 12, 39, 26);
		pnl_Eliminar_Manto.add(button);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 43, 541, 9);
		pnl_Eliminar_Manto.add(separator_1);

		JLabel label_8 = new JLabel("Nombre");
		label_8.setBounds(10, 64, 46, 14);
		pnl_Eliminar_Manto.add(label_8);

		JLabel label_9 = new JLabel("Raza");
		label_9.setBounds(10, 94, 46, 14);
		pnl_Eliminar_Manto.add(label_9);

		JLabel label_10 = new JLabel("Estado");
		label_10.setBounds(10, 124, 46, 14);
		pnl_Eliminar_Manto.add(label_10);

		cboEstado_pEliminar = new JComboBox();
		cboEstado_pEliminar.setModel(new DefaultComboBoxModel(new String[] {
				"0 - Vivo", "1 - Enfermo", "2 - Fallecido" }));
		cboEstado_pEliminar.setBounds(66, 121, 153, 20);
		pnl_Eliminar_Manto.add(cboEstado_pEliminar);

		txtRaza_pEliminar = new JTextField();
		txtRaza_pEliminar.setEnabled(false);
		txtRaza_pEliminar.setColumns(10);
		txtRaza_pEliminar.setBounds(66, 90, 153, 20);
		pnl_Eliminar_Manto.add(txtRaza_pEliminar);

		txtName_pEliminar = new JTextField();
		txtName_pEliminar.setEnabled(false);
		txtName_pEliminar.setColumns(10);
		txtName_pEliminar.setBounds(66, 60, 153, 20);
		pnl_Eliminar_Manto.add(txtName_pEliminar);

		JLabel label_11 = new JLabel("C\u00F3digo de Cliente");
		label_11.setBounds(229, 64, 106, 14);
		pnl_Eliminar_Manto.add(label_11);

		JLabel label_12 = new JLabel("Tipo de animal");
		label_12.setBounds(229, 94, 106, 14);
		pnl_Eliminar_Manto.add(label_12);

		JLabel label_13 = new JLabel("Fecha de nacimiento");
		label_13.setBounds(229, 124, 126, 14);
		pnl_Eliminar_Manto.add(label_13);

		txtFecha_pEliminar = new JTextField();
		txtFecha_pEliminar.setEnabled(false);
		txtFecha_pEliminar.setColumns(10);
		txtFecha_pEliminar.setBounds(365, 117, 189, 20);
		pnl_Eliminar_Manto.add(txtFecha_pEliminar);

		txtTipo_pEliminar = new JTextField();
		txtTipo_pEliminar.setEnabled(false);
		txtTipo_pEliminar.setColumns(10);
		txtTipo_pEliminar.setBounds(365, 88, 189, 20);
		pnl_Eliminar_Manto.add(txtTipo_pEliminar);

		txtCodCliente_pEliminar = new JTextField();
		txtCodCliente_pEliminar.setEnabled(false);
		txtCodCliente_pEliminar.setColumns(10);
		txtCodCliente_pEliminar.setBounds(365, 57, 189, 20);
		pnl_Eliminar_Manto.add(txtCodCliente_pEliminar);

		JLabel label_21 = new JLabel("C\u00F3digo Mascota");
		label_21.setBounds(229, 16, 106, 14);
		pnl_Eliminar_Manto.add(label_21);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				eliminarMascota();
			}
		});
		btnEliminar.setBounds(389, 152, 165, 40);
		pnl_Eliminar_Manto.add(btnEliminar);

		lblNotificar_eliminar = new JLabel("");
		lblNotificar_eliminar.setForeground(Color.RED);
		lblNotificar_eliminar.setBounds(10, 178, 325, 14);
		pnl_Eliminar_Manto.add(lblNotificar_eliminar);

		JPanel pnl_Consultar_Manto = new JPanel();
		tabbedPane.addTab("Consultar", null, pnl_Consultar_Manto, null);
		pnl_Consultar_Manto.setLayout(null);

		JLabel lblConsultarCajeros = new JLabel("Consultar Mascota");
		lblConsultarCajeros.setBounds(10, 11, 179, 22);
		lblConsultarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblConsultarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblConsultarCajeros.setForeground(Color.BLUE);
		lblConsultarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblConsultarCajeros.setAlignmentX(0.5f);
		pnl_Consultar_Manto.add(lblConsultarCajeros);

		JButton btnConsultar = new JButton("b");
		btnConsultar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				buscarMascota_pConsultar();
			}
		});
		btnConsultar.setBounds(491, 11, 39, 26);
		pnl_Consultar_Manto.add(btnConsultar);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(0, 42, 541, 9);
		pnl_Consultar_Manto.add(separator_2);

		JLabel label = new JLabel("Nombre");
		label.setBounds(10, 60, 46, 14);
		pnl_Consultar_Manto.add(label);

		JLabel label_14 = new JLabel("Raza");
		label_14.setBounds(10, 90, 46, 14);
		pnl_Consultar_Manto.add(label_14);

		JLabel label_15 = new JLabel("Estado");
		label_15.setBounds(10, 120, 46, 14);
		pnl_Consultar_Manto.add(label_15);

		cboEstado_pConsultar = new JComboBox();
		cboEstado_pConsultar.setModel(new DefaultComboBoxModel(new String[] {
				"0 - Vivo", "1 - Enfermo", "2 - Fallecido" }));
		cboEstado_pConsultar.setBounds(66, 117, 153, 20);
		pnl_Consultar_Manto.add(cboEstado_pConsultar);

		txtRaza_pConsultar = new JTextField();
		txtRaza_pConsultar.setEditable(false);
		txtRaza_pConsultar.setColumns(10);
		txtRaza_pConsultar.setBounds(66, 86, 153, 20);
		pnl_Consultar_Manto.add(txtRaza_pConsultar);

		txtName_pConsultar = new JTextField();
		txtName_pConsultar.setEditable(false);
		txtName_pConsultar.setColumns(10);
		txtName_pConsultar.setBounds(66, 56, 153, 20);
		pnl_Consultar_Manto.add(txtName_pConsultar);

		JLabel label_16 = new JLabel("C\u00F3digo de Cliente");
		label_16.setBounds(229, 60, 106, 14);
		pnl_Consultar_Manto.add(label_16);

		JLabel label_17 = new JLabel("Tipo de animal");
		label_17.setBounds(229, 90, 106, 14);
		pnl_Consultar_Manto.add(label_17);

		JLabel label_18 = new JLabel("Fecha de nacimiento");
		label_18.setBounds(229, 120, 117, 14);
		pnl_Consultar_Manto.add(label_18);

		txtFecha_pConsultar = new JTextField();
		txtFecha_pConsultar.setEditable(false);
		txtFecha_pConsultar.setColumns(10);
		txtFecha_pConsultar.setBounds(369, 114, 185, 20);
		pnl_Consultar_Manto.add(txtFecha_pConsultar);

		txtTipo_pConsultar = new JTextField();
		txtTipo_pConsultar.setEditable(false);
		txtTipo_pConsultar.setColumns(10);
		txtTipo_pConsultar.setBounds(369, 85, 185, 20);
		pnl_Consultar_Manto.add(txtTipo_pConsultar);

		txtCodCliente_pConsultar = new JTextField();
		txtCodCliente_pConsultar.setEditable(false);
		txtCodCliente_pConsultar.setColumns(10);
		txtCodCliente_pConsultar.setBounds(369, 54, 185, 20);
		pnl_Consultar_Manto.add(txtCodCliente_pConsultar);

		JLabel label_19 = new JLabel("C\u00F3digo Mascota");
		label_19.setBounds(229, 12, 106, 14);
		pnl_Consultar_Manto.add(label_19);

		txtCodMascota_pConsultar = new JTextField();
		txtCodMascota_pConsultar.setColumns(10);
		txtCodMascota_pConsultar.setBounds(345, 11, 116, 20);
		pnl_Consultar_Manto.add(txtCodMascota_pConsultar);

		lblNotificar_consultar = new JLabel("");
		lblNotificar_consultar.setForeground(Color.RED);
		lblNotificar_consultar.setBounds(21, 178, 325, 14);
		pnl_Consultar_Manto.add(lblNotificar_consultar);

		JPanel pnl_Listar_Manto = new JPanel();
		tabbedPane.addTab("Listar", null, pnl_Listar_Manto, null);
		pnl_Listar_Manto.setLayout(null);

		JLabel lblListarCajeros = new JLabel("Listar Mascota");
		lblListarCajeros.setHorizontalTextPosition(SwingConstants.CENTER);
		lblListarCajeros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarCajeros.setForeground(Color.BLUE);
		lblListarCajeros.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListarCajeros.setAlignmentX(0.5f);
		lblListarCajeros.setBounds(10, 11, 140, 22);
		pnl_Listar_Manto.add(lblListarCajeros);

		JButton btnNewButton_1 = new JButton("Listar");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				listarMascota();
			}
		});
		btnNewButton_1.setBounds(173, 40, 89, 23);
		pnl_Listar_Manto.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 74, 544, 118);
		pnl_Listar_Manto.add(scrollPane);

		txtS = new JTextArea();
		txtS.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		txtS.setEditable(false);
		scrollPane.setViewportView(txtS);

		txtCodMascota_pIngresar.setText(generarIDMascota() + "");

		setVisible(false);
	}

	public void actionPerformed(ActionEvent e) {
	}

	public void registrarMascota() {
		String name, tipo, raza, fecNaci, codClienteStr;
		int codMascota, codCliente, estado = -1;
		codMascota = Integer.parseInt(txtCodMascota_pIngresar.getText());
		name = txtNameMascota_pIngresar.getText();
		tipo = txtTipoMascota_pIngresar.getText();
		raza = txtRazaMascota_pIngresar.getText();
		fecNaci = txtFechaNacMascota_pIngresar.getText();
		codClienteStr = cboCliente_pIngresar.getSelectedItem().toString();
		codCliente = Integer.parseInt(codClienteStr);
		switch (cboEstadoMascota_pIngresar.getSelectedIndex()) {
		case 0:
			estado = 0;
			break;
		case 1:
			estado = 1;
			break;
		case 2:
			estado = 2;
			break;
		}
		Mascota objM = new Mascota(codMascota, codCliente, name, tipo, raza,
				fecNaci, estado);
		aMascota.agregar(objM);
		aMascota.grabarMascota();
		limpiarPNL_ingresar();
		notificarIngresar("Mascota " + codMascota + " registrada");
	}

	public void buscarMascota_pModificar() {
		int codi = Integer.parseInt(txtCodMascota_pModificar.getText());
		Mascota objM = aMascota.buscar(codi);
		if (objM == null) {
			notificarModificar("Mascota " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			txtName_pModificar.setText(objM.getNameMascota());
			txtRaza_pModificar.setText(objM.getRaza());
			txtTipo_pModificar.setText(objM.getTipo());
			txtFecha_pModificar.setText(objM.getFechaNacimiento());
			switch (objM.getEstado()) {
			case 0:
				cboEstado_pModificar.setSelectedIndex(0);
				break;
			case 1:
				cboEstado_pModificar.setSelectedIndex(1);
				break;
			case 2:
				cboEstado_pModificar.setSelectedIndex(2);
				break;
			}
			for (int i = 0; i < aCliente.tama�o(); i++) {
				if (objM.getCodCliente() == aCliente.obtener(i).getCodCliente()) {
					cboCliente_pModificar.setSelectedIndex(i);
				}
			}
			notificarModificar("Mascota " + codi + " encontrado");
		}
	}

	public void actualizarMascota() {
		int codi = Integer.parseInt(txtCodMascota_pModificar.getText());
		String codCli;
		Mascota objC = aMascota.buscar(codi);
		if (objC == null) {
			notificarModificar("Mascota " + codi + " NO existe");
			limpiarPNL_modificar();
		} else {
			objC.setNameMascota(txtName_pModificar.getText());
			objC.setFechaNacimiento(txtFecha_pModificar.getText());
			objC.setRaza(txtRaza_pModificar.getText());
			objC.setTipo(txtTipo_pModificar.getText());
			switch (cboEstado_pModificar.getSelectedIndex()) {
			case 0:
				objC.setEstado(0);
				break;
			case 1:
				objC.setEstado(1);
				break;
			}
			codCli = (cboCliente_pModificar.getSelectedItem()).toString();
			objC.setCodCliente(Integer.parseInt(codCli));
			aMascota.grabarMascota();
			notificarModificar("Mascota " + codi + " actualizado");
		}
	}

	public void buscarMascota_pEliminar() {
		int codi = Integer.parseInt(txtCodMascota_pEliminar.getText());
		Mascota objM = aMascota.buscar(codi);
		if (objM == null) {
			notificarEliminar("Mascota " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			txtName_pEliminar.setText(objM.getNameMascota());
			txtRaza_pEliminar.setText(objM.getRaza());
			txtTipo_pEliminar.setText(objM.getTipo());
			txtFecha_pEliminar.setText(objM.getFechaNacimiento());
			txtCodCliente_pEliminar.setText(objM.getCodCliente() + "");
			switch (objM.getEstado()) {
			case 0:
				cboEstado_pEliminar.setSelectedIndex(0);
				break;
			case 1:
				cboEstado_pEliminar.setSelectedIndex(1);
				break;
			case 2:
				cboEstado_pEliminar.setSelectedIndex(2);
				break;
			}
			notificarEliminar("Mascota " + codi + " encontrado");
		}
	}

	public void eliminarMascota() {
		int codi = Integer.parseInt(txtCodMascota_pEliminar.getText());
		Mascota objM = aMascota.buscar(codi);
		if (objM == null) {
			notificarEliminar("Mascota " + codi + " NO existe");
			limpiarPNL_eliminar();
		} else {
			aMascota.eliminar(objM);
			aMascota.grabarMascota();
			notificarEliminar("Mascota " + codi + " eliminado");
		}
	}

	public void buscarMascota_pConsultar() {
		int codi = Integer.parseInt(txtCodMascota_pConsultar.getText());
		Mascota objM = aMascota.buscar(codi);
		if (objM == null) {
			notificarConsultar("Mascota " + codi + " NO existe");
			limpiarPNL_consultar();
		} else {
			txtName_pConsultar.setText(objM.getNameMascota());
			txtRaza_pConsultar.setText(objM.getRaza());
			txtTipo_pConsultar.setText(objM.getTipo());
			txtFecha_pConsultar.setText(objM.getFechaNacimiento());
			txtCodCliente_pConsultar.setText(objM.getCodCliente() + "");
			switch (objM.getEstado()) {
			case 0:
				cboEstado_pConsultar.setSelectedIndex(0);
				break;
			case 1:
				cboEstado_pConsultar.setSelectedIndex(1);
				break;
			case 2:
				cboEstado_pConsultar.setSelectedIndex(2);
				break;
			}
			notificarConsultar("Mascota " + codi + " encontrado");
		}
	}

	public void listarMascota() {
		String linea = "", estado = "";
		txtS.setText("");
		txtS.append("C�d. Mascota \tCod. Cliente \tNom. Mascota\tTipo \tRaza \tFecha Nac. \tEstado\n");
		txtS.append("=====================================================================================\n");
		for (int i = 0; i < aMascota.tama�o(); i++) {
			switch (aMascota.obtener(i).getEstado()) {
			case 0:
				estado = "Vivo - 0";
				break;
			case 1:
				estado = "Enfermo - 1";
				break;
			case 2:
				estado = "Fallecido - 2";
				break;
			}
			linea = aMascota.obtener(i).getCodCliente() + "\t"
					+ aMascota.obtener(i).getCodCliente() + "\t"
					+ aMascota.obtener(i).getNameMascota() + " \t "
					+ aMascota.obtener(i).getTipo() + "\t "
					+ aMascota.obtener(i).getRaza() + "\t "
					+ aMascota.obtener(i).getFechaNacimiento() + " \t "
					+ estado + "\n";
			txtS.append(linea);
		}
	}

	public void limpiarPNL_ingresar() {
		txtCodMascota_pIngresar.setText(generarIDMascota() + "");
		txtNameMascota_pIngresar.setText("");
		txtFechaNacMascota_pIngresar.setText("");
		txtRazaMascota_pIngresar.setText("");
		txtTipoMascota_pIngresar.setText("");
		cboCliente_pIngresar.setSelectedIndex(-1);
		cboEstadoMascota_pIngresar.setSelectedIndex(-1);
	}

	public void limpiarPNL_modificar() {
		txtCodMascota_pModificar.setText("");
		txtName_pModificar.setText("");
		txtFecha_pModificar.setText("");
		txtRaza_pModificar.setText("");
		txtTipo_pModificar.setText("");
		cboCliente_pModificar.setSelectedIndex(-1);
		cboEstado_pModificar.setSelectedIndex(-1);
	}

	public void limpiarPNL_eliminar() {
		txtCodMascota_pEliminar.setText("");
		txtName_pEliminar.setText("");
		txtFecha_pEliminar.setText("");
		txtRaza_pEliminar.setText("");
		txtTipo_pEliminar.setText("");
		txtCodCliente_pEliminar.setText("");
		cboEstado_pEliminar.setSelectedIndex(-1);
	}

	public void limpiarPNL_consultar() {
		txtCodMascota_pConsultar.setText("");
		txtName_pConsultar.setText("");
		txtFecha_pConsultar.setText("");
		txtRaza_pConsultar.setText("");
		txtTipo_pConsultar.setText("");
		txtCodCliente_pConsultar.setText("");
		cboEstado_pConsultar.setSelectedIndex(-1);
	}

	public int generarIDMascota() {
		int canti = aMascota.tama�o();
		int id;
		if (canti == 0) {
			id = 1;
		} else {
			id = canti + 1;
		}
		return id;
	}

	public void notificarIngresar(String s) {
		lblNotificar_ingresar.setText(s);
	}

	public void notificarModificar(String s) {
		lblNotificar_modificar.setText(s);
	}

	public void notificarEliminar(String s) {
		lblNotificar_eliminar.setText(s);
	}

	public void notificarConsultar(String s) {
		lblNotificar_consultar.setText(s);
	}
}
