package View;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class P_1_Principal extends JPanel implements ActionListener {

	public JButton btnCita = new JButton("Citas");
	public JButton btnAtencion = new JButton("Atenci\u00F3n");
	private DemoGUI demoGUI;
	public final JButton btnMantenimieto = new JButton("Mantenimiento");
	public final JButton btnSalir = new JButton("Salir");
	public final JButton btnReporte = new JButton("Reportes");

	public P_1_Principal(final DemoGUI demoGUI) {
		setSize(new Dimension(400, 380));// nadaa
		setBounds(new Rectangle(0, 0, 400, 380));// nadaa
		this.demoGUI = demoGUI;

		// demoGUI.setSize(600, 900);
		setSize(300, 380);// tama�o del panel
		setBackground(Color.WHITE);
		setLayout(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Men\u00FA Principal", TitledBorder.LEADING, TitledBorder.TOP,
				null, Color.BLACK));

		btnCita.addActionListener(this);
		btnCita.setBounds(24, 126, 250, 50);
		add(btnCita);

		btnAtencion.addActionListener(this);
		btnAtencion.setBounds(24, 186, 250, 50);
		add(btnAtencion);

		JLabel lblMenPrincipal = new JLabel("Men\u00FA Principal");
		lblMenPrincipal.setHorizontalAlignment(SwingConstants.CENTER);
		lblMenPrincipal.setForeground(Color.BLUE);
		lblMenPrincipal.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblMenPrincipal.setBounds(82, 24, 141, 31);
		add(lblMenPrincipal);
		btnMantenimieto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				demoGUI.habilita(DemoGUI.PANEL_1_1);
			}
		});
		btnMantenimieto.setBounds(24, 66, 250, 50);

		add(btnMantenimieto);
		btnSalir.setBounds(24, 306, 250, 50);

		add(btnSalir);
		btnReporte.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				demoGUI.habilita(DemoGUI.PANEL_1_4);
			}
		});
		btnReporte.setBounds(24, 246, 250, 50);

		add(btnReporte);

		setVisible(false);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnMantenimieto) {
			demoGUI.habilita(DemoGUI.PANEL_1_1);
		}
		if (e.getSource() == btnCita) {
			demoGUI.habilita(DemoGUI.PANEL_1_2);
		}
		if (e.getSource() == btnAtencion) {
			demoGUI.habilita(DemoGUI.PANEL_1_3);
		}
		if (e.getSource() == btnReporte) {
			demoGUI.habilita(DemoGUI.PANEL_1_4);
		}
	}
}
